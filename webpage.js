var webpage = function(){
	var screenshots = require('protractor-take-screenshots-on-demand');
	var usernameInput = element(by.id('i_username'));
	var passwordInput = element(by.id('i_password'));
	var useraccount = element(by.xpath('/html/body/div[1]/div[2]/div[1]/div/div[3]/div[2]/h2'));
	var add_po = element.all(by.id('i_create_item'));
	var add_AA = element.all(by.id('create_form_btn'));
	var pencil = element.all(by.xpath('//*[@id="i_item_grid"]/table/tbody/tr[1]/td[11]/a/span'));
	var tab_path = '//*[@id="i_item_grid"]/table/tbody/tr[1]/';
	var st = element(by.xpath(tab_path + 'td[9]/span'));
/*--------------------------------------------------------------------LOG IN & LOGOUT--------------------------------------------------------------------*/
	
	//Access link TWMS
	this.getlink = async function(link_TWMS){	
		await browser.get(link_TWMS);
	}; 
	 
	//send Username
	this.name_pass = async function(username,passwd,tc,i){
		await usernameInput.sendKeys(username);
		await browser.sleep(1000);
		await passwordInput.sendKeys(passwd);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Login_test');
	};
	
	//click Login button
	this.loginclick = async function(){
		await element(by.buttonText('Log in')).click().then(function(){
			console.log('Log in success');
		});
	};
	
	//Get User account
	this.getaccount = async function(tc,i){
		await useraccount.getText().then(function(text){
			console.log("User account is \'"+text+"\'");
		});
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Check_account');
		return useraccount.getText();
	};
	
	//Log Out
	this.getout = async function(tc,i){
		await element(by.className('user-profile dropdown-toggle ng-binding')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Logout_test');
		await element.all(by.linkText("Log Out")).click().then(function(){
			console.log('Log out success');
		});
	};
	
/*--------------------------------------------------------------------GO TO MENU : Inbound--------------------------------------------------------------------*/
	
	//Page Purchase Orders
	this.PO_page = async function(tc,i){
  
		//click Purchase Orders
		await element(by.css('[ng-click="changeUrl(\'/purchaseorders\')"]')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Purchase Orders');
		
		//log to console when function is success
		await console.log('Function Go to page Purchase Orders : Success');
	};
	
	//Page Arrival Advices
	this.AA_page = async function(tc,i){
		
		//click Arrival Advices
		await element(by.css('[ng-click="changeUrl(\'/prearrivaladvises\')"]')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Arrival Advices');
  
		//log to console when function is success
		await console.log('Function Go to page Arrival Advices : Success');
	};
	
	//Page Product Receipt
	this.PR_page = async function(tc,i){
		
		//click Arrival Advices
		await element(by.css('[ng-click="changeUrl(\'/receivearrivaladvice\')"]')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Product Receipt');
		
		//log to console when function is success
		await console.log('Function Go to page Product Receipt : Success');
	};
	
	//Page Print Pallet Tag
	this.PT_page = async function(tc,i){
		
		//click Arrival Advices
		await element(by.css('[ng-click="changeUrl(\'/printpallettag\')"]')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Print Pallet Tag');
		
		//log to console when function is success
		await console.log('Function Go to page Print Pallet Tag : Success');
	};
	
/*--------------------------------------------------------------------GO TO MENU : Inventory--------------------------------------------------------------------*/
	
	//Page Stock Tracking
	this.STtrack_page = async function(tc,i){
  
		//Click Inventory
		await element.all(by.linkText('Inventory')).click();
        
		//Click Stock Tracking
        await browser.sleep(2000);
		await element(by.linkText('Stock Tracking')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Stock Tracking');
		
		//log to console when function is success
		await console.log('Function Go to page Stock Tracking : Success');
	};
	//Page Track and Trace
	this.tat_page = async function(tc,i){
		
		//Click Inventory
		await element.all(by.linkText('Inventory')).click();
		
		//Click Track and Trace
		await browser.sleep(2000);
		await element(by.linkText('Track and Trace')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Track and Trace');
		
		//log to console when function is success
		await console.log('Function Go to page Track and Trace : Success');
	};
	//Page Confirm Put Away Location
	this.CPAL_page = async function(tc,i){
		
		//Click Inventory
		await element.all(by.linkText('Inventory')).click();
		
		//Click Confirm Put Away Location
		await browser.sleep(2000);
		await element(by.linkText('Confirm Put Away Location')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Confirm Put Away Location');
  
		//log to console when function is success
		await console.log('Function Go to page Confirm Put Away Location : Success');
	};
	//Page Create Transfer Request
	this.CTR_page = async function(tc,i){
		
		//Click Inventory
		await element.all(by.linkText('Inventory')).click();
		
		//Click Confirm Put Away Location
		await browser.sleep(2000);
		await element(by.linkText('Create Transfer Request')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Create Transfer Request');
  
		//log to console when function is success
		await console.log('Function Go to page Create Transfer Request : Success');
	};
	
/*--------------------------------------------------------------------GO TO MENU : Outbound--------------------------------------------------------------------*/

	//Page Sales Orders
	this.SO_page = async function(tc,i){
  
		//Click Outbound
		await element.all(by.linkText('Outbound')).click();
        
		//Click Queue management
        await browser.sleep(3000);
		await element(by.linkText('Sales Orders')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Sales Orders');
		
		//log to console when function is success
		await console.log('Function Go to page Sales Orders : Success');
	};
	//Page Wave
	this.wave_page = async function(tc,i){
  
		//Click Outbound
		await element.all(by.linkText('Outbound')).click();
        
		//Click Queue management
        await browser.sleep(3000);
		await element.all(by.linkText('Wave')).get(0).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Wave');
		
		//log to console when function is success
		await console.log('Function Go to page Wave : Success');
	};
	//Page Assign Job
	this.AJ_page = async function(tc,i){

		//Click Outbound
		await element.all(by.linkText('Outbound')).click();
        
		//Click Queue management
        await browser.sleep(3000);
		await element(by.linkText('Assign Job')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Assign Job');
		
		//log to console when function is success
		await console.log('Function Go to page Assign Job : Success');
	};
	//Page Queue management
	this.QM_page = async function(tc,i){
  
		//Click Outbound
		await element.all(by.linkText('Outbound')).click();
        
		//Click Queue management
        await browser.sleep(3000);
		await element(by.linkText('Queue management')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Queue management');
		
		//log to console when function is success
		await console.log('Function Go to page Queue management : Success');
	};
	//Page Picking
	this.picking_page = async function(tc,i){
  
		//Click Outbound
		await element.all(by.linkText('Outbound')).click();
        
		//Click Picking
        await browser.sleep(3000);
		await element(by.linkText('Picking')).click();
		await browser.sleep(1000);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Page Picking');
		
		//log to console when function is success
		await console.log('Function Go to page Picking : Success');
	};
	
/*--------------------------------------------------------------------Purchase Orders--------------------------------------------------------------------*/	
	
	//Create Purchase Orders => INITIAL
	this.create_PO_initial = async function(client_path,ref_PO_No,PO_No,doc_date,del_date,sup,pro_num,pd,qty_pd,tc,i){
		
		//Click Add new record for Create PO 
		await add_po.click().then(function(){
			browser.sleep(1000);
			console.log('Add new PO clicked');
			screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Create PO form');
			
			//Click dropdown client
			element(by.css('[aria-owns="i_client_listbox"]')).click();
			
			//Choose Client
			browser.sleep(2000);
			element.all(by.xpath('//*[@id="i_client_listbox"]/'+client_path)).click();
			
			//Ref PO No.
			element(by.id('i_ref_po_no')).sendKeys(ref_PO_No);
			
			//PO No.
			element(by.id('i_po_no')).sendKeys(PO_No);
			
			//Document Date 
			element(by.id('i_document_date')).clear();
			element(by.id('i_document_date')).sendKeys(doc_date);
			
			//Delivery Date
			element(by.id('i_request_date')).clear();
			element(by.id('i_request_date')).sendKeys(del_date);
			
			//Supplier Code
			element(by.id('i_supplier_code_insert')).sendKeys(sup);
			
			//Loop for Add Product item to PO
			for(let n=0;n<pro_num;n++){
				
				//Click button : Add new record
				element.all(by.id('i_create_line_item')).click();
				
				//Product code
				element.all(by.name('productcode')).sendKeys(pd[n]);
				browser.sleep(3000);
				element.all(by.xpath('//*[@id="productcode_dd"]/div[2]/ul/li/span')).click();
				
				//Product Qty
				browser.sleep(1000);
				element(by.xpath('//*[@id="i_lineitem_grid"]/table/tbody/tr/td[4]/span/span/input[1]')).click();
				element(by.name('qtyOrdered')).clear();
				browser.sleep(1000);
				element(by.xpath('//*[@id="i_lineitem_grid"]/table/tbody/tr/td[4]/span/span/input[1]')).click();
				element(by.name('qtyOrdered')).sendKeys(qty_pd[n]);
				
				//Click button : Update
				browser.sleep(1000);
				element.all(by.className('k-icon k-update')).click();
				
			}
			//Click button : Save 
			element(by.id('i_save_po_button')).click();
			
			//PO Alert
			var po_alert = browser.switchTo().alert();
			
			//Check Text in PO alert
			browser.sleep(1000);
			expect(po_alert.getText()).toEqual('Are you sure you want to save this record ?');
			
			//Click OK
			po_alert.accept();
			screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Save PO');
		});
		
		await console.log('Function Create Purchase Orders Status Initial : Success');
	};
	
	//Search PO : Status => INITIAL
	this.search_PO_IN = async function(path_st_INITIAL,PO_No){
		
		await element.all(by.css('[aria-owns="i_status_listbox"]')).click().then(function(){
			
			browser.sleep(1000);
			element.all(by.xpath(path_st_INITIAL)).click();
			
			//Search from PO No.
			browser.sleep(1000);
			element.all(by.id('i_po_no_search')).sendKeys(PO_No);
			
			//Click search button
			browser.sleep(1000);
			element.all(by.css('[ng-click="searchItems()"]')).click();
		});
	};
	
	//Search PO : Status => CONFIRMED
	this.search_PO_CON = async function(path_st_CONFIRMED,PO_No){
		
		await element.all(by.css('[aria-owns="i_status_listbox"]')).click().then(function(){
			
			browser.sleep(1000);
			element.all(by.xpath(path_st_CONFIRMED)).click();
			
			//Search from PO No.
			browser.sleep(1000);
			element(by.id('i_po_no_search')).clear();
			element(by.id('i_po_no_search')).sendKeys(PO_No);
			
			//Click search button
			browser.sleep(2000);
			element.all(by.css('[ng-click="searchItems()"]')).click();
		});
	};
	
	//Confirm PO => CONFIRMED
	this.confirm_PO = async function(tc,i){
		
		//Click PO pencil icon
		await pencil.click().then(function(){
	
			//click confirm PO
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Confirm PO');
			element.all(by.css('[ng-click="confirmPo()"]')).click();
			
		});
		await console.log('Function Create Purchase Orders Status Confirmed : Success');
		
	};
	
	//Get PO No.
	this.get_PO = async function(tc,i,j){
		
		var PO_number = element.all(by.xpath(tab_path + 'td[5]/span'));
		await PO_number.getText().then(function(text){
			console.log("PO No. is \'"+text+"\'");	
		});
		screenshots.takeScreenshot(tc+'-'+i+'-'+j+'. '+'Search PO');
		return PO_number.get(0).getText();
	};
	
	//Get PO Status
	this.get_st_PO = async function(){

		await st.getText().then(function(text){
			console.log("Status of PO No. is \'"+text+"\'");	
		});
		return st.getText();
	};
	
/*--------------------------------------------------------------------Arrival Advices--------------------------------------------------------------------*/		
	
	//Create Arrival Advices => AA_INITIAL
	this.create_AA_initial = async function(client_path,doc_no,sup,doc_date_cal,PO_No,tc,i){
		
		//Click Add new record for Create AA
		await add_AA.click().then(function(){
			console.log('Add new AA clicked');
			browser.sleep(1000);
			screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Create AA form');
			
			//Click dropdown client
			element(by.css('[aria-owns="i_client_name_edit_listbox"]')).click();
			
			//Choose Client
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="i_client_name_edit_listbox"]/'+client_path)).click();
			
			//Click warehouse dropdown
			element.all(by.css('[aria-owns="i_warehouse_edit_listbox"]')).click();
			browser.sleep(1000);
			
			//Choose warehouse
			element.all(by.xpath('//*[@id="i_warehouse_edit_listbox"]/li[1]')).click();
			
			//Doc. No.
			element.all(by.id('i_shipment_no_edit')).sendKeys(doc_no);
			
			//Search supplier
			element.all(by.id('i_shipper_modal')).click();
			browser.sleep(1000);
			element.all(by.id('i_search_supplier_code_insert')).sendKeys(sup);
			element.all(by.id('SEARCH_insert')).click();
			browser.sleep(1000);
			
			//Click supplier
			element.all(by.xpath('//*[@id="grid_shipper_insert"]/div[2]/table/tbody/tr/td[3]/a/span')).click();
			
			//Document date
			browser.executeScript(doc_date_cal);
			
			//Click button : Add Product from PO
			browser.sleep(1000);
			element.all(by.id('create_add_po_item_btn')).click();
			
			//Search PO
			browser.sleep(1000);
			element.all(by.id('i_refDocumentNo_search')).sendKeys(PO_No);
			element.all(by.id('b_po_search')).click();
			
			//Choose PO
			browser.sleep(8000);
			element.all(by.xpath('//*[@id="grid_po"]/table/tbody/tr/td[9]/a/span')).click();
			
			//Save & Confirm
			browser.sleep(5000);
			element.all(by.id('i_button_save_and_confirm')).click();
			
			//AA Alert
			browser.sleep(2000);
			var aa_alert = browser.switchTo().alert();
			
			//Check Text in AA alert
			expect(aa_alert.getText()).toEqual('Are you sure you want to Save ?');
			
			//Click OK
			aa_alert.accept();
			screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Save AA');
		});
		await console.log('Function Create Arrival Advices Status AA_INITIAL : Success');
	};
	
	//Search AA : Status => AA_INITIAL
	this.search_AA_CON = async function(path_st_CONFIRMED,doc_no){
		
		//Search AA
		await element.all(by.id('i_shipment_date')).clear();
		await browser.sleep(1000);
		await element.all(by.id('i_document_no')).sendKeys(doc_no);
		
		//Status CONFIRMED
		await element.all(by.css('[aria-owns="i_status_listbox"]')).click();
		await browser.sleep(1000);
		await element.all(by.xpath(path_st_CONFIRMED)).click();
			
		//Click Search button
		await element.all(by.id('search_btn')).click();
			
	};
	
	//Get Doc. No.
	this.get_doc = async function(tc,i,j){
		
		var doc_number = element.all(by.xpath(tab_path + 'td[2]/a'));
		await doc_number.getText().then(function(text){
			console.log("Doc. No. of this AA is \'"+text+"\'");	
		});
		screenshots.takeScreenshot(tc+'-'+i+'-'+j+'. '+'Search AA');
		return doc_number.get(0).getText();
	};
	
	//Get AA Status
	this.get_st_AA = async function(){

		await st.getText().then(function(text){
			console.log("Status of this AA is \'"+text+"\'");	
		});
		return st.getText();
	};
	
	//Get AA Status
	this.get_st_AA = async function(){
		await st.getText().then(function(text){
			console.log("Status of this AA is \'"+text+"\'");	
		});
		return st.getText();
	};

/*--------------------------------------------------------------------Product Receipt--------------------------------------------------------------------*/		

	//Product Receipt on web
	this.pr = async function(mfg_pd,exp_pd,doc_no,val_st,tc,i){
		
		//Choose AA for Product Receipt
		await element.all(by.xpath('//*[@id="i_items"]/table/tbody/tr/td[9]')).click();
		
		//Click Receive button
		await browser.sleep(3000);
		await screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Choose AA for receive');
		await element.all(by.id('i_button_receive')).click().then(function(){
			browser.sleep(3000);
			screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Product Receive form');
			
			//Count table rows to find number of product items
			var table = element.all(by.id('itemGridOptionsLineGrDoc'));
			var row = table.all(by.css('tr.ng-scope')).count().then(function(cnt){
				
				var item = cnt/2;
				console.log('Number of product items is ' + item);
				
				//Loop for receipt product item
				for(let n=1;n<=item;n++){
					
					var t_path = '/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[3]/div/div/div[3]/form/div[4]/div/table/tbody/tr['+ (n*2) +']/';
					var qty_tg = element(by.xpath(t_path+'td[5]'));
					var mfg = element(by.xpath(t_path+'td[7]/span/span[1]/span/input'));
					var exp = element(by.xpath(t_path+'td[8]/span/span[1]/span/input'));
					var stty = element(by.xpath('//*[@id="itemGridOptionsLineGrDoc"]/table/tbody/tr['+(n*2)+']/td[9]/span/span/span[2]/span'));
					var lot_but = element.all(by.buttonText('LOT'));
					var ser_but = element.all(by.buttonText('Serial No.'));
					
					//Check Qty. Target
					browser.sleep(1000);
					var qty_rc = qty_tg.getText().then(function(text){
						return text;
					});
					
					//Receive Qty.
					browser.sleep(2000);
					element(by.xpath(t_path+'td[6]/input')).sendKeys(qty_rc);
					
					//MFG Date
					browser.sleep(1000);
					mfg.click().sendKeys(mfg_pd[item-n]);
					
					//EXP Date
					browser.sleep(1000);
					exp.isEnabled().then(function(result){
						if(result){
							browser.sleep(1000);
							exp.click();
							browser.sleep(1000);
							exp.sendKeys(exp_pd[item-n]);
						}
					});
					
					//Status
					browser.sleep(1000);
					stty.click().then(function(){
						browser.sleep(2000);
						var div = ((n-1)*3)+15;
						element.all(by.xpath('/html/body/div['+div+']/div/div[2]/ul/li['+val_st+']')).click();
					});
					
					//Lot 
					lot_but.get(n-1).isDisplayed().then(function(result){
						if(result){
							browser.sleep(1000);
							lot_but.get(n-1).click();
							browser.sleep(1000);
							element.all(by.id('i_lot_no')).get(0).sendKeys(doc_no);
							element(by.css('[ng-click="saveLot()"]')).click();
						}
					});
					
					//Serial Number
					ser_but.get(n-1).isDisplayed().then(function(result){
						if(result){
							browser.sleep(1000);
							ser_but.get(n-1).click();
							browser.sleep(3000);
							element.all(by.id('i_lot_no')).get(1).sendKeys(doc_no);
							element(by.css('[ng-click="saveSerialNo()"]')).click();
						}
					});
					
				}
			
			});
			//Click button : Save
			browser.sleep(2000);
			element(by.id('confirm_receive')).click();
			
			//Product Receipt Alert
			browser.sleep(2000);
			var pr_alert = browser.switchTo().alert();
			
			//Check Text in Product Receipt alert
			expect(pr_alert.getText()).toEqual('Are you sure you want to CONFIRM RECEIVE ?');
			
			//Click OK
			pr_alert.accept();
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-3. '+'Save Product receipt');
		});
		await console.log('Function Product Receipt : Success');
	};
	
	//Product Receipt received on web 
	this.pr_received = async function(mfg_pd,exp_pd_re,doc_no,val_st,tc,i){
		
		//Choose AA for Product Receipt
		await element.all(by.xpath('//*[@id="i_items"]/table/tbody/tr/td[9]')).click();
		
		//Click Receive button
		await browser.sleep(3000);
		await screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Choose AA for receive');
		await element.all(by.id('i_button_receive')).click().then(function(){
			browser.sleep(3000);
			screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Product Receive form');
			
			//Count table rows to find number of product items
			var table = element.all(by.id('itemGridOptionsLineGrDoc'));
			var row = table.all(by.css('tr.ng-scope')).count().then(function(cnt){
				
				var item = cnt/2;
				console.log('Number of product items is ' + item);
				
				//Loop for receipt product item
				for(let n=1;n<=item;n++){
					
					var t_path = '/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[3]/div/div/div[3]/form/div[4]/div/table/tbody/tr['+ (n*2) +']/';
					var qty_tg = element(by.xpath(t_path+'td[5]'));
					var mfg = element(by.xpath(t_path+'td[7]/span/span[1]/span/input'));
					var exp = element(by.xpath(t_path+'td[8]/span/span[1]/span/input'));
					var stty = element(by.xpath('//*[@id="itemGridOptionsLineGrDoc"]/table/tbody/tr['+(n*2)+']/td[9]/span/span/span[2]/span'));
					var lot_but = element.all(by.buttonText('LOT'));
					var ser_but = element.all(by.buttonText('Serial No.'));
					
					//Check Qty. Target
					browser.sleep(1000);
					var qty_rc = qty_tg.getText().then(function(text){
						return text;
					});
					
					//Receive Qty.
					browser.sleep(2000);
					element(by.xpath(t_path+'td[6]/input')).sendKeys(qty_rc);
					
					//MFG Date
					browser.sleep(1000);
					mfg.click().sendKeys(mfg_pd[item-n]);
					
					
					//EXP Date
					browser.sleep(1000);
					exp.isEnabled().then(function(result){
						if(result){
							browser.sleep(1000);
							exp.click();
							browser.sleep(1000);
							exp.sendKeys(exp_pd_re[item-n]);
						}
					});
					
					//Status
					browser.sleep(1000);
					stty.click().then(function(){
						browser.sleep(2000);
						var div = ((n-1)*3)+15;
						element.all(by.xpath('/html/body/div['+div+']/div/div[2]/ul/li['+val_st+']')).click();
					});
					
					//Lot
					lot_but.get(n-1).isDisplayed().then(function(result){
						if(result){
							browser.sleep(1000);
							lot_but.get(n-1).click();
							browser.sleep(1000);
							element.all(by.id('i_lot_no')).get(0).sendKeys(doc_no);
							element(by.css('[ng-click="saveLot()"]')).click();
						}
					});
					
					//Serial Number
					ser_but.get(n-1).isDisplayed().then(function(result){
						if(result){
							browser.sleep(1000);
							ser_but.get(n-1).click();
							browser.sleep(3000);
							element.all(by.id('i_lot_no')).get(1).sendKeys(doc_no);
							element.all(by.id('i_serial_no')).get(0).sendKeys(doc_no);
							element(by.css('[ng-click="saveSerialNo()"]')).click();
						}
					});
					
				}
			
			});
			//Click button : Save
			browser.sleep(2000);
			element(by.id('confirm_receive')).click();
			
			//Product Receipt Alert
			browser.sleep(2000);
			var pr_alert = browser.switchTo().alert();
			
			//Check Text in Product Receipt alert
			expect(pr_alert.getText()).toEqual('Are you sure you want to CONFIRM RECEIVE ?');
			
			//Click OK
			pr_alert.accept();
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-3. '+'Save Product receipt');
			
			//Error Modal
			browser.sleep(10000);
			element.all(by.id('errorByModal')).isEnabled().then(function(result){
				if(result){		
					expect(element(by.xpath('//*[@id="errorByModal"]/div/div/div[2]/div/div[1]/div/label')).getText()).toEqual('Do you want to confirm this receipt ?').then(function(){
						screenshots.takeScreenshot(tc+'-'+i+'-4. '+'Some items have problems');
						element(by.id('saveConfirm')).click();
						console.log('Status Product Receipt is RECEIVED');
					});
				}
			});
		});
		await console.log('Function Product Receipt : Success');
	};
	
	//Product Adjust or Reject
	this.ad_re = async function(mode,account,Test_date,tc,i){
		
		//Choose AA for Product Adjust
		await element.all(by.xpath('//*[@id="i_items"]/table/tbody/tr/td[9]')).click().then(function(){
			browser.sleep(30000);
			screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Choose AA for adjust');
			
			//Click button : View
			element.all(by.xpath('//*[@id="i_itemGr"]/table/tbody/tr/td[6]/a/button')).click();
		
			if(mode == 'ad'){
				//Click button : Adjust
				browser.sleep(2000);
				screenshots.takeScreenshot(tc+'-'+i+'-2. '+'AA detail');
				element.all(by.css('[ng-click="popupAdjust()"]')).click();
			
				//Name :
				browser.sleep(3000);
				screenshots.takeScreenshot(tc+'-'+i+'-3. '+'Adjust form');
				element.all(by.id('i_customer_name')).first().sendKeys(account);
			
				//Tel :
				browser.sleep(2000);
				element.all(by.id('i_customer_tel')).first().sendKeys(Test_date);
			
				//Click button : Save
				browser.sleep(1000);
				element(by.id('saveAdjust')).click();
				
				//Adjust Alert
				browser.sleep(2000);
				var aj_alert = browser.switchTo().alert();
			
				//Check Text in Adjust alert
				expect(aj_alert.getText()).toEqual('Are you sure you want to ADJUST RECEIVE ?');
			
				//Click OK
				aj_alert.accept();
				screenshots.takeScreenshot(tc+'-'+i+'-4. '+'Save adjust');
			}
			else if(mode == 're'){
				//Click button : Reject
				browser.sleep(2000);
				screenshots.takeScreenshot(tc+'-'+i+'-2. '+'AA detail');
				element.all(by.css('[ng-click="popupReject()"]')).click();
			
				//Name :
				browser.sleep(3000);
				screenshots.takeScreenshot(tc+'-'+i+'-3. '+'Reject form');
				element.all(by.id('i_customer_name')).get(1).sendKeys(account);
			
				//Tel :
				browser.sleep(2000);
				element.all(by.id('i_customer_tel')).get(1).sendKeys(Test_date);
			
				//Click button : Save
				browser.sleep(1000);
				element(by.id('saveReject')).click();
				
				//Reject Alert
				browser.sleep(2000);
				var rj_alert = browser.switchTo().alert();
			
				//Check Text in Adjust alert
				expect(rj_alert.getText()).toEqual('Are you sure you want to REJECT CONFIRM ?');
			
				//Click OK
				rj_alert.accept();
				screenshots.takeScreenshot(tc+'-'+i+'-4. '+'Save reject');
			}
			
		});
		if(mode == 'ad'){
			await console.log('Function Adjust Product Receipt Status COMPLETED : Success');
		}
		else if(mode == 're'){
			await console.log('Function Reject Product Receipt Status AA_INITIAL : Success');
		}
	};
	//Search AA Status
	this.search_PR_all = async function(doc_no){
		
		//Document No.
		await element.all(by.id('i_document_no_edit')).clear().sendKeys(doc_no);
		
		//Status RECEIVED
		await element.all(by.css('[aria-owns="listStatus_listbox"]')).click();
		await browser.sleep(2000);
		await element.all(by.xpath('//*[@id="listStatus_listbox"]/li[1]')).click();
		
		//Click button : Search
		await browser.sleep(1000);
		await element.all(by.id('i_btn_search')).click()
		
	};
	
	//Search AA : Status => AA_INITIAL
	this.search_PR_AA = async function(doc_no,doc_date){
		
		//Document No.
		await element.all(by.id('i_document_no_edit')).sendKeys(doc_no);
		
		//Doc Date
		await element.all(by.id('i_start_date')).sendKeys(doc_date);
		
		//Status AA_INITIAL
		await element.all(by.css('[aria-owns="listStatus_listbox"]')).click();
		await browser.sleep(2000);
		await element(by.xpath('//*[@id="listStatus_listbox"]/li[3]')).click();
		
		//Click button : Search
		await browser.sleep(1000);
		await element.all(by.id('i_btn_search')).click();
	};
	//Search AA : Status => RECEIVED
	this.search_PR_received = async function(doc_no){
		
		//Document No.
		await element.all(by.id('i_document_no_edit')).sendKeys(doc_no);
		
		//Status RECEIVED
		await element.all(by.css('[aria-owns="listStatus_listbox"]')).click();
		await browser.sleep(2000);
		await element.all(by.xpath('//*[@id="listStatus_listbox"]/li[4]')).click();
		
		//Click button : Search
		await browser.sleep(1000);
		await element.all(by.id('i_btn_search')).click()
		
	};
	
	//Search AA : Status => COMPLETED
	this.search_PR_completed = async function(doc_no){
		
		//Document No.
		await element.all(by.id('i_document_no_edit')).clear().sendKeys(doc_no);
		
		//Status RECEIVED
		await element.all(by.css('[aria-owns="listStatus_listbox"]')).click();
		await browser.sleep(2000);
		await element.all(by.xpath('//*[@id="listStatus_listbox"]/li[7]')).click();
		
		//Click button : Search
		await browser.sleep(1000);
		await element.all(by.id('i_btn_search')).click()
		
	};
	
	//Get Status in AA detail page 
	this.PR_st_detail = async function(tc,i,j){

		var st_detail = element.all(by.xpath('//*[@id="i_itemGr"]/table/tbody/tr/td[5]/span'));

		await st_detail.getText().then(function(text){
			console.log("Status in AA detail page is \'"+text+"\'");	
		});
		await screenshots.takeScreenshot(tc+'-'+i+'-'+j+'. '+'Status in AA detail page');
		return st_detail.get(0).getText();
	};

	//Get Status Product Receipt in search page 
	this.PR_st_search = async function(tc,i){

		var st_search = element.all(by.xpath('//*[@id="i_items"]/table/tbody/tr[1]/td[8]/span'));
		
		await st_search.getText().then(function(text){
			console.log("Status Product Receipt in search page is \'"+text+"\'");	
		});
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Search Product receipt');
		return st_search.get(0).getText();
	};
	
	//Get AA number
	this.get_aa = async function(){
		var AA_number = element(by.binding('dataItem.documentNo'));
		await AA_number.getText().then(function(text){
			console.log("AA number is \'"+text+"\'");
		});
		return AA_number.getText();
	};
	
/*--------------------------------------------------------------------Print Pallet Tag--------------------------------------------------------------------*/		
	
	//Search Pallet Tag by AA number
	this.search_PT = async function(AA_PT){
		
		//Search by AA number
		await element(by.id('i_shipment_no')).sendKeys(AA_PT);
		
		//Click button : Search
		await browser.sleep(3000);
		await element.all(by.id('search_btn')).click();
		
	};
	//Print Pallet tag
	this.print_pt = async function(link,PT_number,tc,i){
		
		//Click Print button
		await element(by.xpath('//*[@id="printGrid"]/table/tbody/tr[2]/td[13]/a/button')).click().then(function(){
			browser.sleep(1000);
			browser.getAllWindowHandles().then(function(handles) {
				
				// Open New tab
				newWindowHandle = handles[1]; 
				browser.switchTo().window(newWindowHandle).then(function () {
					//Expect check url
					expect(browser.getCurrentUrl()).toEqual(link+'pallettagreport?print=true&palletTagId=(' + PT_number +')');
					console.log("Now, We open a new tab");
					browser.sleep(3000);
					screenshots.takeScreenshot(tc+'-'+i+'. '+'Show Pallet Tag');
					// Close tab
					browser.close();
				});
				
				//Go back to T-WMS web page
				oldWindowHandle = handles[0];
				browser.switchTo().window(oldWindowHandle).then(function () {
					//Expect check url
					expect(browser.getCurrentUrl()).toEqual(link+'#/printpallettag');
					console.log("Close tab and Go back to T-WMS Print Pallet Tag page");
				});
			});
		});
		
	};
	
	//Get Pallet Tag
	this.get_pt = async function(){
		var PT_number = await element(by.xpath('//*[@id="printGrid"]/table/tbody/tr[2]/td[2]'));
		await PT_number.getText().then(function(text){
			console.log("Pallet Tag number is \'"+text+"\'");
		});
		return PT_number.getText();
	};
	
	//Get AA text
	this.expect_PT = async function(AA_PT){
		
		var word = await element(by.xpath('//*[@id="printGrid"]/table/tbody/tr[1]/td/p')).getText();
		expect(await word).toContain(AA_PT);
		await console.log('Found Pallet Tag');
		var table = await element(by.id('printGrid'));
		var row = await table.all(by.css('tr.ng-scope')).count().then(function(cnt){
			var pt = cnt-1;
			console.log('Quantity of pallet tag is ' + pt);
		});
		
		/*browser.sleep(10000);
		
		var page = element.all(by.xpath('//*[@id="printGrid"]/div[1]/a[4]'));
		
		
		page.getAttribute('data-page').then(function(pg){
			console.log(pg);
			return pg;
		});
		
		var page_val = parseInt(page) - 1;
		var row = table.all(by.css('tr.ng-scope')).count().then(function(cnt){
			var pt = (15*page_val) + cnt-1;
			console.log('Quantity of pallet tag is ' + pt);
			
		});*/
	};
	

/*--------------------------------------------------------------------Stock Tracking--------------------------------------------------------------------*/	

	//Stock Tracking Search
	this.sttrack_search = async function(client_index,pd_code,PT_ID,lot_no,SN_no,tc,i){
		
		//Client
		await element.all(by.xpath('//*[@id="tabstrip-1"]/form/div[1]/div/div/span/span/span[2]/span')).click();
		await browser.sleep(1000);
		await element.all(by.css('li.k-item.ng-scope')).get(client_index).click();
		
		//Product code search
		await element(by.id('product_code_search')).sendKeys(pd_code);
		
		//Pallet tag id search
		await element(by.id('s_pallet_tag')).sendKeys(PT_ID);
		
		//Lot search
		await element(by.id('s_lot')).sendKeys(lot_no);
		
		//SN(Serial Number) search
		await element(by.id('s_serial_number')).sendKeys(SN_no);
		
		//Click Search button
		await browser.sleep(1000);
		await element.all(by.css('[ng-click="searchItems()"]')).click();
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Search Stock Tracking');
		
		await console.log("Search button clicked");
	};
	//Stock Tracking check first row
	this.sttrack_check = async function(client_index,pd_code,PT_ID,lot_no,SN_no,tc,i){
		
		var word = 'Found Stock Tracking search by';
		var client_word = '';
		if(client_index != '0' && client_index != ''){
			var c_search = element(by.xpath('//*[@id="tabstrip-1"]/form/div[1]/div/div/span/span/span[1]'));
			var first_row_client = element(by.xpath('//*[@id="i_item_grid"]/table/tbody/tr[1]/td[1]'));
			expect(await c_search.getText()).toEqual(first_row_client.getText());
			var cli = await first_row_client.getText().then(function(text){
				return text;
			});
			word = word +'\n-> Client: '+ cli;
		}
		if(pd_code != ''){
			var first_row_pro = element(by.xpath('//*[@id="i_item_grid"]/table/tbody/tr[1]/td[2]'));
			expect(await first_row_pro.getText()).toEqual(pd_code);
			var pc = await first_row_pro.getText().then(function(text){
				return text;
			});
			word = word +'\n-> Product code: '+ pc;
		}
		if(PT_ID != ''){
			var first_row_pt = element(by.xpath('//*[@id="i_item_grid"]/table/tbody/tr[1]/td[7]'));
			expect(await first_row_pt.getText()).toEqual(PT_ID);
			var pt = await first_row_pt.getText().then(function(text){
				return text;
			});
			word = word +'\n-> Pallet tag id: '+ pt;
		}
		if(lot_no != ''){
			var first_row_lot = element(by.xpath('//*[@id="i_item_grid"]/table/tbody/tr[1]/td[4]'));
			expect(await first_row_lot.getText()).toEqual(lot_no);
			var lt = await first_row_lot.getText().then(function(text){
				return text;
			});
			word = word +'\n-> Lot: '+ lt;
		}
		if(SN_no != ''){
			var first_row_SN = element(by.xpath('//*[@id="i_item_grid"]/table/tbody/tr[1]/td[5]'));
			expect(await first_row_SN.getText()).toEqual(SN_no);
			var sn = await first_row_SN.getText().then(function(text){
				return text;
			});
			word = word +'\n-> SN: '+ sn;
		}
		await console.log(word);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Show Stock Tracking result');
	};

/*--------------------------------------------------------------------Track and Trace--------------------------------------------------------------------*/	
	
	//Track and Trace Search
	this.tat_search = async function(client_index,wh_index,pd_code,Aisle,Bay,Level,Keyword,tc,i){
		
		//Client 
		await element.all(by.xpath('//*[@id="tabstrip-1"]/form/div[1]/div[1]/div/span/span/span[1]')).click();
		await browser.sleep(1000);
		await element.all(by.css('li.k-item.ng-scope')).get(client_index).click();
		
		//Warehouse
		await element.all(by.xpath('//*[@id="tabstrip-1"]/form/div[1]/div[2]/div/span/span/span[1]')).click();
		await browser.sleep(1000);
		await element.all(by.xpath('//*[@id="s_warehouse_listbox"]/li['+ wh_index +']')).click();
		
		//Product code search
		await element(by.id('s_product_code')).sendKeys(pd_code);
		
		//Location search
		await element(by.id('s_aisle')).sendKeys(Aisle);
		await element(by.id('s_bay')).sendKeys(Bay);
		await element(by.id('s_level')).sendKeys(Level);
		
		//Keyword search
		await element(by.id('s_keyword')).sendKeys(Keyword);
		
		//Click Search button
		await browser.sleep(1000);
		await element.all(by.css('[ng-click="searchLocator()"]')).click();
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Search Track and Trace');
		
		await console.log("View button clicked");
	};
	
	//Track and Trace check first row
	this.tat_check = async function(client_index,wh_index,pd_code,Aisle,Bay,Level,Keyword,tc,i){
		
		var word = 'Found Track and Trace search by';
		if(client_index != '0' && client_index != ''){
			var c_search = element(by.xpath('//*[@id="tabstrip-1"]/form/div[1]/div[1]/div/span/span/span[1]'));
			var cli = await c_search.getText().then(function(text){
				return text;
			});
			word = word +'\n-> Client: '+ cli;
		}
		if(wh_index != '0' && wh_index != ''){
			var wh_search = element(by.xpath('//*[@id="tabstrip-1"]/form/div[1]/div[2]/div/span/span/span[1]'));
			var first_row_wh = element(by.xpath('//*[@id="itemgrid"]/table/tbody/tr[1]/td[1]'));
			expect(await wh_search.getText()).toEqual(first_row_wh.getText());
			var wh = await first_row_wh.getText().then(function(text){
				return text;
			});
			word = word +'\n-> Warehouse: '+ wh;
		}
		if(pd_code != ''){
			var first_row_pro = element(by.xpath('//*[@id="itemgrid"]/table/tbody/tr[1]/td[3]'));
			expect(await first_row_pro.getText()).toEqual(pd_code);
			var pc = await first_row_pro.getText().then(function(text){
				return text;
			});
			word = word +'\n-> Product code: '+ pc;
		}
		if(Aisle != ''){
			var first_row_ai = element(by.xpath('//*[@id="itemgrid"]/table/tbody/tr[1]/td[2]'));
			var str_lo = await first_row_ai.getText().then(function(text){
				return text;
			});
			var str = str_lo.split('-');
			expect(await parseInt(str[0])).toEqual(parseInt(Aisle));
			word = word +'\n-> Location Aisle: '+ str[0];
		}
		if(Bay != ''){
			var first_row_bay = element(by.xpath('//*[@id="itemgrid"]/table/tbody/tr[1]/td[2]'));
			var str_lo = await first_row_bay.getText().then(function(text){
				return text;
			});
			var str = str_lo.split('-');
			expect(await parseInt(str[1])).toEqual(parseInt(Bay));
			word = word +'\n-> Location Bay: '+ str[1];
		}
		if(Level != ''){
			var first_row_lev = element(by.xpath('//*[@id="itemgrid"]/table/tbody/tr[1]/td[2]'));
			var str_lo = await first_row_lev.getText().then(function(text){
				return text;
			});
			var str = str_lo.split('-');
			expect(await parseInt(str[2])).toEqual(parseInt(Level));
			word = word +'\n-> Location Level: '+ str[2];
		}
		if(Keyword != ''){
			var first_row_lc = element(by.xpath('//*[@id="itemgrid"]/table/tbody/tr[1]/td[2]'));
			var first_row_pt = element(by.xpath('//*[@id="itemgrid"]/table/tbody/tr[1]/td[9]'));
			var first_row_lt = element(by.xpath('//*[@id="itemgrid"]/table/tbody/tr[1]/td[10]'));
			await first_row_lc.getText().then(function(txlc){
				if(txlc.indexOf(Keyword) != -1){
					word = word +'\n-> Keyword: \''+ Keyword +'\' in Locator Code column';
				}
			});
			await first_row_pt.getText().then(function(txpt){
				if(txpt.indexOf(Keyword) != -1){
					word = word +'\n-> Keyword: \''+ Keyword +'\' in Pallet Tag column';
				}
			});
			await first_row_lt.getText().then(function(txlt){
				if(txlt.indexOf(Keyword) != -1){
					word = word +'\n-> Keyword: \''+ Keyword +'\' in Lot column';
				}
			});
			
		}
		await console.log(word);
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Show Track and Trace result');
	};
	
/*--------------------------------------------------------------------Confirm Put Away Location--------------------------------------------------------------------*/	

	//Locator not available Search
	this.lna_search = async function(AA_Doc){
		
		//Document No.
		await element(by.id('i_document_no')).sendKeys(AA_Doc);
		
		//Status : LOCATOR_NOT_AVAILABLE
		await element.all(by.xpath('//*[@id="tabstrip-1"]/form/div[2]/div[1]/span/span/span[1]')).click();
		await browser.sleep(1000);
		await element.all(by.xpath('//*[@id="i_status_listbox"]/li[4]')).click();
		
		//Click Search button
		await browser.sleep(1000);
		await element.all(by.id('search_btn')).click();
		
		//Check string contain AA number or Document No.
		await browser.sleep(8000);
		var word = await element(by.xpath('//*[@id="i_item_grid"]/table/tbody/tr[1]/td/p')).getText();
		expect(word).toContain(AA_Doc);
		console.log('Found Pallet Tag ' + AA_Doc);
		
	};
	//Confirm Put Away Location
	this.CPAL = async function(s_ai,s_ba,s_le,tc,i){
		
		//Check row of data table
		var table = element.all(by.id('i_item_grid'));
		let n=1;
		var row = await table.all(by.css('tr.ng-scope')).count().then(function(cnt){
			var item = cnt-1;
			console.log('Number of rows is ' + item);
			
			for(n;n<=item;n++){
				
				browser.sleep(5000);
				
				var pt = element(by.xpath('//*[@id="i_item_grid"]/table/tbody/tr[2]/td[3]'));
				
				//Click icon edit
				element.all(by.css('[ng-click="CreateLocationModal(dataItem)"]')).get(0).click();
				
				//Select location
				browser.sleep(3000);
				screenshots.takeScreenshot(tc+'-'+i+'-'+n+'-1. '+'Select location form');
				element(by.id('i_location_x_search')).sendKeys(s_ai);
				element(by.id('i_location_y_search')).sendKeys(s_ba);
				element(by.id('i_location_z_search')).sendKeys(s_le);
				element(by.id('b_location_search')).click();
				
				//Click Save button
				browser.sleep(3000);
				screenshots.takeScreenshot(tc+'-'+i+'-'+n+'-2. '+'Select location');
				element.all(by.css('[ng-click="selectLocation(dataItem)"]')).get(0).click();
				
				
				//Confirm Put Away Location Alert
				browser.sleep(2000);
				var cpal_alert = browser.switchTo().alert();
			
				//Check Text in Confirm Put Away Location alert
				expect(cpal_alert.getText()).toEqual('Are you sure you want to comfirm Edit Loc ?');
			
				//Click OK
				cpal_alert.accept();
				
				
				pt.getText().then(function(text){
					console.log("Pallet Tag number \'"+text+"\' is located");
					
				});
				
			}
		});
		await browser.sleep(2000);
		await console.log("Confirm Put Away Location : Success");
		await screenshots.takeScreenshot(tc+'-'+i+'-'+n+'. Confirm Put Away location success');
	};
	//Put away Search
	this.pa_search = async function(AA_Doc){
		
		//Status : PUT_AWAY
		await element.all(by.xpath('//*[@id="tabstrip-1"]/form/div[2]/div[1]/span/span/span[1]')).click();
		await browser.sleep(1000);
		await element.all(by.xpath('//*[@id="i_status_listbox"]/li[5]')).click();
		
		//Click Search button
		await browser.sleep(1000);
		await element.all(by.id('search_btn')).click();
		
		//Check string contain AA number
		await browser.sleep(5000);
		var word = await element(by.xpath('//*[@id="i_item_grid"]/table/tbody/tr[1]/td/p')).getText();
		expect(word).toContain(AA_Doc);
		console.log('Found Pallet Tag ' + AA_Doc);
	};
	//Get Status in search page 
	this.cpal_st_search = async function(tc,i){

		var cpal_search = element.all(by.xpath('//*[@id="i_item_grid"]/table/tbody/tr[2]/td[8]/span'));
		
		await cpal_search.getText().then(function(text){
			console.log("Status in search page is \'"+text+"\'");	
		});
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Search Confirm Put Away Location');
		return cpal_search.get(0).getText();
	};

/*--------------------------------------------------------------------Create Transfer Request--------------------------------------------------------------------*/		

	//Create transfer request : Good -> Good & Good -> Damage
	this.CTR_form = async function(client_path,pro,mm_date,mm_type,wh_from,x_from,y_from,z_from,wh_to,x_to,y_to,z_to,qty_from,su_qty_from,tc,i){
		
		//Click Add transfer request
		await element.all(by.css('[ng-click="showCreateItemForm()"]')).first().click().then(function(){
			browser.sleep(3000);
			
			//Select client
			element(by.css('[aria-owns="insert_client_listbox"]')).click();
			browser.sleep(2000);
			element.all(by.xpath('//*[@id="insert_client_listbox"]/'+client_path)).click();
			
			//Movement Date
			element(by.id('insert_movement_date')).clear();
			element(by.id('insert_movement_date')).sendKeys(mm_date);
			
			//Movement type
			//element.all(by.xpath('//*[@id="create_trasnfer_request_form"]/div[3]/div[1]/div/span/span/span[1]')).click();
			//browser.sleep(2000);
			//var ble = element.all(by.xpath('/html/body/div[11]/div/div[2]/ul'));
			//ble.all(by.className('k-item.ng-scope')).get(0).click();
			//browser.actions().mouseMove(element.all(by.xpath('/html/body/div[11]/div/div[2]/ul/li[1]'))).click().perform();
			//element(by.xpath('/html/body/div[11]/div/div[2]/ul/li[1]')).click();
			//element(by.linkText('Transfer Locator')).click();
			//element.all(by.xpath('//*[@id="create_trasnfer_request_form"]/div[3]/div[1]/div/span/select/option[1]')).click();
			//element.all(by.xpath('//*[@id="create_trasnfer_request_form"]/div[4]/div[3]/div/span/span/span[1]')).click();
			//browser.sleep(10000);
			//element.all(by.xpath('/html/body/div[15]/div/div[2]/ul/li[2]')).click();
			//element(by.xpath('//*[@id="create_trasnfer_request_form"]/div[4]/div[3]/div/span/select/option[value="1"]')).click();
			//element.all(by.css('div.ul.k-list.k-reset.li.k-item.ng-scope')).get(1).click();
			//var elm = element(by.xpath('/html/body/div[11]/div/div[2]/ul/li[1]'));
			//browser.executeScript("arguments[0].click();", elm.getAttribute());
			//element(by.model("subType")).element(by.css("[value='1']")).click();
			//element(by.css('select option:1')).click();
			
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Add transfer request');
			
			//Click Add new record
			element.all(by.css('[ng-click="addTrasnferRequest()"]')).click();
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Create Transfer Request form');
			
			//Product Code
			element(by.id('product_code')).sendKeys(pro);
			browser.sleep(3000);
			element(by.xpath('//*[@id="product_code_listbox"]/li')).click();
			
			//Click list data
			element.all(by.css('[ng-click="listdata()"]')).click();
			browser.sleep(15000);
			
			//From Warehouse
			element(by.css('[aria-owns="warehouse_from_listbox"]')).click();
			browser.sleep(2000);
			element.all(by.xpath('//*[@id="warehouse_from_listbox"]/li[text()="ILS_Warehouse_'+wh_from+'"]')).first().click();
			
			//From Aisle
			browser.sleep(1000);
			element(by.css('[aria-owns="modal_locator_x_listbox"]')).click();
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="modal_locator_x_listbox"]/li[text()="'+x_from+'"]')).click();
			
			//From Bay
			element(by.css('[aria-owns="modal_locator_y_listbox"]')).click();
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="modal_locator_y_listbox"]/li[text()="'+y_from+'"]')).click();
			
			//From Level
			element(by.css('[aria-owns="locator_z_from_listbox"]')).click();
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="locator_z_from_listbox"]/li[text()="'+z_from+'"]')).click();
			
			//Transfer to Warehouse
			element(by.css('[aria-owns="warehouse_to_listbox"]')).click();
			browser.sleep(3000);
			element.all(by.xpath('//*[@id="warehouse_to_listbox"]/li[text()="ILS_Warehouse_'+wh_to+'"]')).first().click();
			
			//Transfer to Aisle
			browser.sleep(1000);
			element(by.css('[aria-owns="modal_locator_x_to_listbox"]')).click();
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="modal_locator_x_to_listbox"]/li[text()="'+x_to+'"]')).click();
			
			//Transfer to Bay
			element(by.css('[aria-owns="modal_locator_y_to_listbox"]')).click();
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="modal_locator_y_to_listbox"]/li[text()="'+y_to+'"]')).click();
			
			//Transfer to Level
			element(by.css('[aria-owns="locator_z_to_listbox"]')).click();
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="locator_z_to_listbox"]/li[text()="'+z_to+'"]')).click();
			
			browser.sleep(2000);
			//From Qty
			if(qty_from !== ''){
				element.all(by.id('qty_from')).get(0).clear().sendKeys(qty_from);
			}
			
			//From SU Qty
			if(su_qty_from !== ''){
				element.all(by.id('su_qty_from')).get(0).clear().sendKeys(su_qty_from);
			}
			
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-3. '+'Save Create Transfer Request');
			
			//Click Save button
			element(by.css('[ng-click="saveTransferRequest()"]')).click();
			
		});
		
		await console.log('Create Transfer Request Status INITIAL : Success');
	};
	//Create transfer request : Damage -> Good & Good -> EXP
	this.CTR_form2 = async function(client_path,pro,mm_date,mm_type,wh_from,x_from,y_from,z_from,wh_to,x_to,y_to,z_to,qty_from,su_qty_from,new_lot,exp_new_lot,mfg_new_lot,tc,i){
		
		//Click Add transfer line
		await element.all(by.css('[ng-click="showCreateItemForm()"]')).first().click().then(function(){
			
			browser.sleep(3000);
			
			//Select client
			element(by.css('[aria-owns="insert_client_listbox"]')).click();
			browser.sleep(2000);
			element.all(by.xpath('//*[@id="insert_client_listbox"]/'+client_path)).click();
			
			//Movement Date
			element(by.id('insert_movement_date')).clear();
			element(by.id('insert_movement_date')).sendKeys(mm_date);
			
			//Movement type
			element.all(by.xpath('//*[@id="create_trasnfer_request_form"]/div[3]/div[1]/div/span/span/span[1]')).click();
			browser.sleep(2000);
			element.all(by.xpath('/html/body/div[11]/div/div[2]/ul/li['+mm_type+']')).click();
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Add transfer line');
			
			//Click Add new record
			element.all(by.css('[ng-click="addTrasnferRequest()"]')).click();
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Create Transfer Request form');
			
			//Product Code
			browser.sleep(2000);
			element(by.id('product_code')).sendKeys(pro);
			browser.sleep(3000);
			element(by.xpath('//*[@id="product_code_listbox"]/li')).click();
			
			//Click list data
			element.all(by.css('[ng-click="listdata()"]')).click();
			browser.sleep(10000);
			
			//From Warehouse
			element(by.css('[aria-owns="warehouse_from_listbox"]')).click();
			browser.sleep(3000);
			element.all(by.xpath('//*[@id="warehouse_from_listbox"]/li[text()="ILS_Warehouse_'+wh_from+'"]')).first().click();
			
			//From Aisle
			browser.sleep(1000);
			element(by.css('[aria-owns="modal_locator_x_listbox"]')).click();
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="modal_locator_x_listbox"]/li[text()="'+x_from+'"]')).click();
			
			//From Bay
			element(by.css('[aria-owns="modal_locator_y_listbox"]')).click();
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="modal_locator_y_listbox"]/li[text()="'+y_from+'"]')).click();
			
			//From Level
			element(by.css('[aria-owns="locator_z_from_listbox"]')).click();
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="locator_z_from_listbox"]/li[text()="'+z_from+'"]')).click();
			
			//Transfer to Warehouse
			element(by.css('[aria-owns="warehouse_to_listbox"]')).click();
			browser.sleep(3000);
			element.all(by.xpath('//*[@id="warehouse_to_listbox"]/li[text()="ILS_Warehouse_'+wh_to+'"]')).first().click();
			
			//Transfer to Aisle
			browser.sleep(1000);
			element(by.css('[aria-owns="modal_locator_x_to_listbox"]')).click();
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="modal_locator_x_to_listbox"]/li[text()="'+x_to+'"]')).click();
			
			//Transfer to Bay
			element(by.css('[aria-owns="modal_locator_y_to_listbox"]')).click();
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="modal_locator_y_to_listbox"]/li[text()="'+y_to+'"]')).click();
			
			//Transfer to Level
			element(by.css('[aria-owns="locator_z_to_listbox"]')).click();
			browser.sleep(1000);
			element.all(by.xpath('//*[@id="locator_z_to_listbox"]/li[text()="'+z_to+'"]')).click();
			
			browser.sleep(2000);
			//From Qty
			if(qty_from !== ''){
				element.all(by.id('qty_from')).get(0).clear().sendKeys(qty_from);
			}
			
			//From SU Qty
			if(su_qty_from !== ''){
				element.all(by.id('su_qty_from')).get(0).clear().sendKeys(su_qty_from);
			}
			
			//Create new lot
			if(new_lot !== ''){
				//Click new lot button
				element(by.css('[ng-click="addNewLOT()"]')).click();
				browser.sleep(2000);
				//Change lot name
				element(by.id('lot_name_insert')).clear().sendKeys(new_lot);
				//Change exp lot
				element(by.id('lot_expire_insert')).clear().sendKeys(exp_new_lot);
				//Change mfg lot
				element(by.id('lot_mfg_insert')).clear().sendKeys(mfg_new_lot);
				//Click save button
				element(by.css('[ng-click="saveNewLOT()"]')).click();
			}
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-3. '+'Save Create Transfer Request');
			element(by.css('[ng-click="saveTransferRequest()"]')).click();
			
		});
		switch(mm_type){
			case '3' :
			console.log('Create Transfer Request DAMAGE->GOOD Status INITIAL : Success');
			break;
		}
	};
	
	//Search Create Transfer Line
	this.tfl_search = async function(client_path,pro,mm_date,mm_type,tc,i){
		
		//Select Client
		await element(by.xpath('//*[@id="tabstrip-1"]/form/div[1]/div[1]/div/span/span/span[1]')).click();
		await browser.sleep(2000);
		await element(by.xpath('/html/body/div[4]/div/div[2]/ul/'+client_path)).click();
		
		//Product code
		await element(by.id('product_code_search')).sendKeys(pro);
		await browser.sleep(3000);
		await element(by.xpath('//*[@id="product_code_search_listbox"]/li[1]')).click();
		
		//Select Status -> INITIAL
		await element(by.css('[aria-owns="search_doc_stat_listbox"]')).click();
		await browser.sleep(2000);
		await element(by.xpath('//*[@id="search_doc_stat_listbox"]/li[2]')).click();
		
		//Select Movement type
		await element(by.xpath('//*[@id="tabstrip-1"]/form/div[3]/div[2]/div/span/span/span[1]')).click();
		await browser.sleep(2000);
		await element.all(by.xpath('/html/body/div[9]/div/div[2]/ul/li['+(parseInt(mm_type)+1)+']')).click();
		
		await screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Search transfer line');
		await element(by.css('[ng-click="searchData()"]')).click();
	}; 
	//Expect check status transfer line
	this.tfl_st_search = async function(tc,i){

		var tfl_st = element.all(by.xpath('//*[@id="grid_search_mtrasnfer"]/table/tbody/tr[1]/td[8]/span'));
		
		await tfl_st.getText().then(function(text){
			console.log("Status transfer line in search page is \'"+text+"\'");	
		});
		await screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Check Transfer line Status');
		return tfl_st.get(0).getText();
	};
	//Delete transfer line
	this.tfl_delete = async function(tc,i){
		
		await element(by.xpath('//*[@id="grid_search_mtrasnfer"]/table/tbody/tr[1]/td[10]/a/span')).click().then(function(){
			browser.sleep(3000);
			var box = element(by.xpath('//*[@id="modelDelItem"]/div/div/div[2]/div/div/div/label'));
			expect(box.getText()).toEqual('Confirm to Cancel Document ?');
			element(by.css('[ng-click="submitDelete()"]')).click();
			browser.sleep(4000);
			screenshots.takeScreenshot(tc+'-'+i+'. '+'Delete Transfer Request');
			var noti = element.all(by.css('div.ui-pnotify-text'));
			expect(noti.get(0).getText()).toEqual('Delete success');
		});
		
		await console.log("Delete transfer line : Success");
	};
	
/*--------------------------------------------------------------------Sales Orders--------------------------------------------------------------------*/		

	//Create Sales Orders
	this.create_so = async function(client_path,ship_met,customer,ship_to,so_date,rq_date,ref_doc_no,so_pro_num,so_pd,so_qty_pd,tc,i){
		
		await element(by.css('[ng-click="showCreateItemForm()"]')).click().then(function(){
			console.log('Create SO clicked');
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Create Sales Orders form');
			
			//Select Client
			element(by.css('[aria-owns="i_client_listbox"]')).click();
			browser.sleep(2000);
			element(by.xpath('//*[@id="i_client_listbox"]/'+client_path)).click();
			browser.sleep(2000);
			
			//Select Shipping Method
			element(by.css('[aria-owns="i_shiping_method_insert_listbox"]')).click();
			browser.sleep(2000);
			element(by.xpath('//*[@id="i_shiping_method_insert_listbox"]/'+ship_met)).click();
			
			//Select Customer
			element(by.id('i_customer_insert')).sendKeys(customer);
			browser.sleep(5000);
			element(by.xpath('//*[@id="i_customer_insert_listbox"]/li[1]')).click();
			
			//Select Ship to address
			browser.sleep(5000);
			element(by.css('[aria-owns="i_shipto_address_insert_listbox"]')).click();
			browser.sleep(2000);
			element(by.xpath('//*[@id="i_shipto_address_insert_listbox"]/'+ship_to)).click();
			
			//SO Date
			element(by.id('i_document_date')).sendKeys(so_date);
			
			//Request Date
			element(by.id('i_request_date_insert')).sendKeys(rq_date);
			
			//Ref Doc No.
			element(by.id('i_ref_doc_insert')).sendKeys(ref_doc_no);
			
			//Loop for Add Product item to PO
			for(let n=0;n<so_pro_num;n++){
				
				//Click Create line item button
				element.all(by.id('i_create_line_item')).click();
				
				//Product code
				element.all(by.name('productcode')).sendKeys(so_pd[n]);
				browser.sleep(5000);
				element.all(by.xpath('/html/body/div[19]/div/div[2]/ul/li/span')).get(0).click();
				
				//Product Qty
				browser.sleep(3000);
				element(by.xpath('//*[@id="i_lineitem_grid"]/table/tbody/tr/td[4]/span/span/input[1]')).click();
				element(by.name('qtyOrdered')).clear();
				browser.sleep(1000);
				element(by.xpath('//*[@id="i_lineitem_grid"]/table/tbody/tr/td[4]/span/span/input[1]')).click();
				element(by.name('qtyOrdered')).sendKeys(so_qty_pd[n]);
				
				//Click button : Update
				browser.sleep(1000);
				element.all(by.className('k-icon k-update')).click();
			}
			//Click button : Save&Confirm
			element(by.buttonText('Save&Confirm')).click();
			
			//SO Alert
			browser.sleep(1000);
			var so_alert = browser.switchTo().alert();
			
			//Check Text in SO alert
			expect(so_alert.getText()).toEqual('finish');
			
			//Click OK
			so_alert.accept();
			screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Save&Confirm SO');
		});
		
		await console.log('Function Create Sales Orders Status CONFIRMED : Success');
	};
	//Search SO in Search page
	this.so_st_search = async function(ref_doc_no,rq_date,so_date,tc,i){
		
		//Search by Ref Doc No.
		element(by.id('i_ref_doc_number')).sendKeys(ref_doc_no);
		
		//Select Status -> COMPLETED
		element(by.css('[aria-owns="i_status_listbox"]')).click();
		browser.sleep(2000);
		element(by.xpath('//*[@id="i_status_listbox"]/li[4]')).click();
		
		//Search by Request date.
		element(by.id('i_request_date')).sendKeys(rq_date);
		
		//Search by SO date.
		element(by.id('i_created_date')).sendKeys(so_date);
		
		//Click Search button
		screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Search SO');
		element.all(by.css('[ng-click="searchItems()"]')).get(0).click();
		
		
	};
	
	//Get Status Sales Orders in search page 
	this.so_st_check = async function(tc,i){

		var so_check = element.all(by.xpath('//*[@id="itemGridOptions"]/div[3]/table/tbody/tr/td[12]/span'));
		
		await so_check.get(0).getText().then(function(text){
			console.log("Status Sales Orders in search page is \'"+text+"\'");	
		});
		screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Check SO Status');
		return so_check.get(0).getText();
	};
	
/*--------------------------------------------------------------------Wave--------------------------------------------------------------------*/		

	//Create Wave
	this.create_wave = async function(c_index,wh_pick,w_config,pk_date,tc,i){
		
		//Click Add new wave
		await element(by.css('[ng-click="addnewwave()"]')).click().then(function(){
			console.log('Add new wave clicked');
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Add new wave');
			
			//Select Client
			element(by.css('[aria-owns="s_main_client_listbox"]')).click();
			browser.sleep(2000);
			element(by.xpath('//*[@id="s_main_client_listbox"]/li['+c_index+']')).click();
		
			//Select Warehouse
			if(wh_pick!='' && wh_pick!='0'){
				browser.sleep(1000);
				element(by.css('[aria-owns="warehouse2_listbox"]')).click();
				browser.sleep(2000);
				element(by.xpath('//*[@id="waveconfig2_listbox"]/li[text()="ILS_Warehouse_'+wh_pick+'"]')).click();
			}
			
			//Select Wave Config
			element(by.css('[aria-owns="waveconfig2_listbox"]')).click();
			browser.sleep(3000);
			element(by.xpath('//*[@id="waveconfig2_listbox"]/li['+w_config+']')).click();
		
			//Pick date
			browser.sleep(1000);
			element(by.id('pickdate2')).sendKeys(pk_date);
			
			//Click Save button
			browser.sleep(1000);
			screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Save wave');
			element(by.css('[ng-click="create()"]')).click();
			
		});
		
		await console.log('Function Create Wave Status INITIAL : Success');
	};
	//Search Wave INITIAL in Search page
	this.wave_st_initial = async function(c_index,wh_pick,pk_date,tc,i){
		
		//Select Client
		await element(by.xpath('//*[@id="tabstrip-1"]/form/div[2]/div[1]/div/span/span/span[1]')).click();
		await browser.sleep(2000);
		await element(by.xpath('/html/body/div[5]/div/div[3]/ul/li['+c_index+']')).click();
		
		//Select Warehouse
		if(wh_pick!='' && wh_pick!='0'){
			await browser.sleep(1000);
			await element(by.css('[aria-owns="warehouse1_listbox"]')).click();
			await browser.sleep(2000);
			await element(by.xpath('//*[@id="warehouse1_listbox"]/li[text()="ILS_Warehouse_'+wh_pick+'"]')).click();
		}
		
		//Select Status -> INITIAL
		await element(by.css('[aria-owns="status1_listbox"]')).click();
		await browser.sleep(2000);
		await element(by.xpath('//*[@id="status1_listbox"]/li[2]')).click();
		
		//Date From
		await element(by.id('datefrom1')).sendKeys(pk_date);
		
		//Date To
		await element(by.id('dateto1')).clear();
		
		//Click search button
		await element.all(by.css('[ng-click="searchWave()"]')).get(0).click().then(function(){
			console.log("Search button clicked");
		});
		await screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Search Wave');
	};	
	//Search Wave that planned
	this.wave_st_plan = async function(stp,c_index,wh_pick,pk_date,tc,i){
		
		//Select Client
		await element(by.xpath('//*[@id="tabstrip-1"]/form/div[2]/div[1]/div/span/span/span[1]')).click();
		await browser.sleep(2000);
		await element.all(by.xpath('/html/body/div[6]/div/div[3]/ul/li['+c_index+']')).click();
		
		//Select Warehouse
		if(wh_pick!='' && wh_pick!='0'){
			await browser.sleep(1000);
			await element(by.css('[aria-owns="warehouse1_listbox"]')).click();
			await browser.sleep(2000);
			await element(by.xpath('//*[@id="warehouse1_listbox"]/li[text()="ILS_Warehouse_'+wh_pick+'"]')).click();
		}
		
		let pst=0;
		if(stp=='PLANNING'){pst=1;}
		else{pst=4;}
		
		//Select Status
		await element(by.css('[aria-owns="status1_listbox"]')).click();
		await browser.sleep(2000);
		await element(by.xpath('//*[@id="status1_listbox"]/li['+pst+']')).click();
		
		//Date From
		await element(by.id('datefrom1')).sendKeys(pk_date);
		
		//Date To
		await element(by.id('dateto1')).clear();
		
		//Click search button
		await element.all(by.css('[ng-click="searchWave()"]')).get(0).click().then(function(){
			console.log("Search button clicked");
		});
		await screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Search Wave');
	};
	//Search Wave that released
	this.wave_st_released = async function(c_index,wh_pick,pk_date,tc,i){
		
		//Select Client
		await element(by.xpath('//*[@id="tabstrip-1"]/form/div[2]/div[1]/div/span/span/span[1]')).click();
		await browser.sleep(2000);
		await element.all(by.xpath('/html/body/div[5]/div/div[3]/ul/li['+c_index+']')).click();
		
		//Select Warehouse
		if(wh_pick!='' && wh_pick!='0'){
			await browser.sleep(1000);
			await element(by.css('[aria-owns="warehouse1_listbox"]')).click();
			await browser.sleep(2000);
			await element(by.xpath('//*[@id="warehouse1_listbox"]/li[text()="ILS_Warehouse_'+wh_pick+'"]')).click();
		}
		
		//Select Status
		await element(by.css('[aria-owns="status1_listbox"]')).click();
		await browser.sleep(2000);
		await element(by.xpath('//*[@id="status1_listbox"]/li[4]')).click();
		
		//Date From
		await element(by.id('datefrom1')).sendKeys(pk_date);
		
		//Date To
		await element(by.id('dateto1')).clear();
		
		//Click search button
		await element.all(by.css('[ng-click="searchWave()"]')).get(0).click().then(function(){
			console.log("Search button clicked");
		});
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Search RELEASED Wave');
	};	
	//Get Status Wave in search page 
	this.wave_st_check = async function(tc,i){

		var wave_check = element.all(by.xpath('//*[@id="searchgrid"]/table/tbody/tr/td[7]/span'));
		
		await wave_check.get(0).getText().then(function(text){
			console.log("Status Wave in search page is \'"+text+"\'");	
		});
		await screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Wave Status');
		return wave_check.get(0).getText();
	};
	//Create Wave
	this.wave_add_so = async function(so_num,ref_doc_no,rq_date,customer,tc,i){
		
		//Click View button
		await element.all(by.css('[ng-click="viewwave(dataItem)"]')).get(0).click().then(function(){
			console.log('View button clicked');
			browser.sleep(1000);
			screenshots.takeScreenshot(tc+'-'+i+'-1. '+'SO detail');
			
			//Click Add SO
			element(by.css('[ng-click="addso()"]')).click();
        
			//Ref Doc No.
			if(so_num == '1'){
				browser.sleep(3000);
				element(by.id('sono3')).sendKeys(ref_doc_no);
			}
			
			//Select SO Status -> CONFIRMED
			browser.sleep(2000);
			element(by.css('[aria-owns="sostatus3_listbox"]')).click()
			browser.sleep(2000);
			element(by.xpath('//*[@id="sostatus3_listbox"]/li[2]')).click();
		
			//Req. Date
			element(by.id('reqdate3')).sendKeys(rq_date);
		
			//Customer
			element(by.id('customer3')).sendKeys(customer);
			screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Search SO');
			
			//Click Search button
			element(by.css('[ng-click="searchSO()"]')).click();
			
			//Wait for loading data
			browser.sleep(100000);
			
			//Select SO
			for(let i=1;i<=so_num;i++){
				browser.sleep(1000);
				var so_no = element(by.xpath('//*[@id="sogrid"]/div[2]/table/tbody/tr['+i+']/td[4]'));
				element(by.xpath('//*[@id="sogrid"]/div[2]/table/tbody/tr['+i+']/td[1]/input')).click();
				so_no.getText().then(function(text){
					console.log("Select SO No. \'"+text+"\' to wave.");
				});
			}
			
			//Click Add button
			browser.sleep(1000);
			screenshots.takeScreenshot(tc+'-'+i+'-3. '+'Add SO');
			element(by.css('[ng-click="addSoToWave()"]')).click();
			
			//Click Save button
			browser.sleep(20000);
			screenshots.takeScreenshot(tc+'-'+i+'-4. '+'Save SO');
			element(by.css('[ng-click="create()"]')).click();
			
		});
		await console.log('Add So to Wave : Success');
	};
	
	//Find pallet number on Wave
	this.find_pt = async function(pallet_number,tc,i){
		
		//Click View button
		await element.all(by.css('[ng-click="viewwave(dataItem)"]')).get(0).click().then(function(){
			console.log('View button clicked');
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-1. '+'SO detail');
			
			//Click Start Plan button
			element(by.css('[ng-click="startplan()"]')).click();
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Start Plan page');
			
			//Click Attr button
			browser.sleep(10000);
			element.all(by.css('[ng-click="showCreatePickingModal(dataItem)"]')).get(0).click();
			browser.sleep(2000);
			screenshots.takeScreenshot(tc+'-'+i+'-3. '+'Edit Product Attribute form');
			
			//Input Pallet Number
			element(by.id('i_picking_palletNo')).sendKeys(pallet_number);
			browser.sleep(1000);
			element(by.css('[ng-click="searchStorage()"]')).click();
			browser.sleep(1000);
			screenshots.takeScreenshot(tc+'-'+i+'-4. '+'Search Pallet Number');
			
		});
		
	};
	//Check Find pallet number on Wave
	this.ch_find_pt = async function(pallet_number,tc,i){
		
		//Check Pallet number from first row
		var first_row_pt = element(by.xpath('//*[@id="storageGrid"]/div[2]/table/tbody/tr[1]/td[9]'));
		await first_row_pt.getText().then(function(text){
			expect(first_row_pt.getText()).toEqual(pallet_number);
			console.log("Found Pallet Number \'"+text+"\' in check stock table");
		});
		await screenshots.takeScreenshot(tc+'-'+i+'-5. '+'Pallet Number in check stock table');
		
		await browser.sleep(2000);
		//Click close button
		await element(by.xpath('//*[@id="pickingModal"]/div/div/div[1]/button/span')).click();
	};
	//View to check status in wave
	this.ciw = async function(tc,i){
		
		//Click View button
		await element.all(by.css('[ng-click="viewwave(dataItem)"]')).get(0).click().then(function(){
			browser.sleep(3000);
			var table = element.all(by.id('wavegrid'));
			var row = table.all(by.css('tr.ng-scope')).count().then(function(cnt){
				console.log('Number of SO in wave is ' + cnt);
				if(cnt == 1){
					element(by.xpath('//*[@id="wavegrid"]/table/tbody/tr/td[5]/span')).getText().then(function(text){
						console.log("SO Status in wave is \'"+text+"\'");
					});
				}
				else if(cnt > 1){
					for(let n=1;n<=cnt;n++){
						element(by.xpath('//*[@id="wavegrid"]/table/tbody/tr['+n+']/td[5]/span')).getText().then(function(text){
							console.log("SO number ["+n+"] in wave, Status is \'"+text+"\'");
						});
					}
				}
				
			});
		});
		screenshots.takeScreenshot(tc+'-'+i+'. '+'View SO Status');
	};
	//Get Doc. No.
	this.get_docno = async function(){
		var DN_number = element(by.xpath('//*[@id="searchgrid"]/table/tbody/tr[1]/td[4]'));
		await DN_number.getText().then(function(text){
			console.log("Doc. No. number is \'"+text+"\'");
		});
		return DN_number.getText();
	};
	
/*--------------------------------------------------------------------Picking--------------------------------------------------------------------*/		
	
	//Search Picking
	this.picking_search = async function(c_index,wh_pick,pk_date,dn,tc,i){
		
		//Select Client
		await element(by.css('[aria-owns="listClient_listbox"]')).click();
		await browser.sleep(2000);
		await element.all(by.xpath('//*[@id="listClient_listbox"]/li['+c_index+']')).click();
		
		//Select Warehouse
		if(wh_pick!='' && wh_pick!='0'){
			await browser.sleep(1000);
			await element(by.css('[aria-owns="warehouse_listbox"]')).click();
			await browser.sleep(2000);
			await element(by.xpath('//*[@id="warehouse_listbox"]/li[text()="ILS_Warehouse_'+wh_pick+'"]')).click();
		}
		
		//Select Status
		await element(by.css('[aria-owns="status_listbox"]')).click();
		await browser.sleep(2000);
		await element(by.xpath('//*[@id="status_listbox"]/li[7]')).click();
		
		//Pick Date
		await element(by.id('pickDate1')).sendKeys(pk_date);
		
		//Keyword
		await element(by.id('i_keyword')).sendKeys(dn);
		
		//Click search button
		await element.all(by.css('[ng-click="searchPicking()"]')).get(0).click().then(function(){
			console.log("Search button clicked");
		});
		await screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Search Picking');
	};	
	//Get Status Picking in search page 
	this.pick_st_check = async function(tc,i){

		var pick_check = element.all(by.xpath('//*[@id="itemGridOptions"]/div[2]/table/tbody/tr[1]/td[12]/span'));
		
		await pick_check.get(0).getText().then(function(text){
			console.log("Status Picking in search page is \'"+text+"\'");	
		});
		await screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Picking Status');
		return pick_check.get(0).getText();
	};
	//Get Picking Doc No.
	this.get_jobkw = async function(){
		var pdn_number = element(by.xpath('//*[@id="itemGridOptions"]/div[2]/table/tbody/tr[1]/td[5]'));
		await pdn_number.getText().then(function(text){
			console.log("Picking Doc. No. is \'"+text+"\'");
		});
		return pdn_number.getText();
	};
	
/*--------------------------------------------------------------------Assign Job--------------------------------------------------------------------*/		
	
	//Assign Job
	this.assign_job = async function(picker,client_job,pk_status,pick_date,job_keyword,job,tc,i){
		
		//Click New Assign Job button
		await element(by.css('[ng-click="showCreateItemForm()"]')).click().then(function(){
			console.log("New Assign Job clicked");
			
			//Select Client
			browser.sleep(3000);
			element(by.xpath('//*[@id="i_tab_type_detail"]/div[2]/div[1]/div[2]/span')).click();
			browser.sleep(3000);
			if(job==1 && pick_date != ''){
				element(by.xpath('/html/body/div[38]/div/div[2]/ul/li['+client_job+']')).click();
			}
			else{
				element(by.xpath('/html/body/div[9]/div/div[2]/ul/li['+client_job+']')).click();
			}
			
			//Select Pick Status
			browser.sleep(3000);
			element(by.css('[aria-owns="status1_listbox"]')).click();
			browser.sleep(3000);
			if(job==1 && pick_date != ''){
				element(by.xpath('/html/body/div[39]/div/div[2]/ul/li['+pk_status+']')).click();
			}
			else{
				element(by.xpath('//*[@id="status1_listbox"]/li['+pk_status+']')).click();
			}
			
			//Pick Date
			element(by.id('i_assignjob_date')).sendKeys(pick_date);
			
			//Keyword for Select Picking
			element(by.model('search2.keyword')).sendKeys(job_keyword);
			
			screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Search Job to pick');
			//Click Search button
			element(by.css('[ng-click="searchfunction()"]')).click();
			browser.sleep(15000);
			
			//Username for Select Picker
			element(by.id('user_search')).sendKeys(picker);
			browser.sleep(2000);
			element(by.css('[ng-click="userchange()"]')).click();
			browser.sleep(15000);
			
			//Count table row
			var table = element.all(by.id('itemGridOptions2'));
			var row = table.all(by.css('tr.ng-scope')).count().then(function(cnt){
				
				for(let n=1; n<=job ;n++){
					var apk;
					if(cnt == 1){
						apk = element(by.xpath('//*[@id="itemGridOptions2"]/div[2]/table/tbody/tr/td[4]'));
					}
					else{
						apk = element(by.xpath('//*[@id="itemGridOptions2"]/div[2]/table/tbody/tr['+n+']/td[4]'));
					}
					
					//Select Job
					if(cnt == 1){
						element(by.xpath('//*[@id="itemGridOptions2"]/div[2]/table/tbody/tr/td[1]/input')).click();
					}
					else{
						element(by.xpath('//*[@id="itemGridOptions2"]/div[2]/table/tbody/tr['+n+']/td[1]/input')).click();
					}
					
					apk.getText().then(function(text){
						console.log("Picking No. \'"+text+"\' is selected");
					});
				}
				//Click Add Job button
				browser.sleep(3000);
				element(by.css('[ng-click="addjob()"]')).click();
				
				//Check add job success
				browser.sleep(10000);
				var box1 = element.all(by.css('div.ui-pnotify-text')).get(0);
				expect(box1.getText()).toEqual('add job success');
				
			});
		});
		await console.log("Add job : Success");
		await screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Add job');
	};
	//Delete Assign Job
	this.delete_aj = async function(job,tc,i){
		//Count table row
		var tablej = element.all(by.id('itemGridOptions3'));
		var rowj = await tablej.all(by.css('tr.ng-scope')).count().then(function(cnt){
				
			for(let n=1; n<=job ;n++){
				var dapk;
				if(cnt == 1){
					dapk = element(by.xpath('//*[@id="itemGridOptions3"]/div[2]/table/tbody/tr/td[2]'));
				}
				else{
					dapk = element(by.xpath('//*[@id="itemGridOptions3"]/div[2]/table/tbody/tr['+n+']/td[2]'));
				}
				//Select Job
				if(cnt == 1){
					element(by.xpath('//*[@id="itemGridOptions3"]/div[2]/table/tbody/tr/td[1]/input')).click();
				}
				else{
					element(by.xpath('//*[@id="itemGridOptions3"]/div[2]/table/tbody/tr['+n+']/td[1]/input')).click();
				}
				dapk.getText().then(function(text){
					console.log("Picking-Ref Doc. \'"+text+"\' is selected");
				});
				screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Re-assign job');
			}
			//Click Add Job button
			browser.sleep(2000);
			element(by.css('[ng-click="save()"]')).click();
			
			//Check add job success
			browser.sleep(5000);
			var box2 = element.all(by.css('div.ui-pnotify-text')).get(0);
			expect(box2.getText()).toEqual('save success');
			
		});
		await console.log("Delete job : Success");
		await screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Delete job');
	};
	
/*--------------------------------------------------------------------Queue management--------------------------------------------------------------------*/		
	
	//Plan Wave
	this.plan_wave = async function(cli_ent,wave_num,waveid,tc,i){
		
		//Select client
		await element(by.xpath('//*[@id="tabstrip-1"]/form/div[1]/div/div/span/span/span[1]')).click();
		await browser.sleep(5000);
		let selectcli_ent = element(by.xpath('/html/body/div[26]/div/div[2]/ul/li['+cli_ent+']'));
		if(i==4){
			selectcli_ent = element.all(by.css('li.k-item.ng-scope')).get(cli_ent-1);
		}
		//await element.all(by.css('li.k-item.ng-scope')).get(cli_ent-1).click().then(function(){
			//html/body/div[49]/div/div[2]/ul/li[5]
		await selectcli_ent.click().then(function(){
			browser.sleep(20000);
			screenshots.takeScreenshot(tc+'-'+i+'-1. '+'Wave in Pending list');
			var box = element.all(by.id('sortable-listC'));
			
			//Count wave in Pending list box
			var wave = box.all(by.css('li.list-item.ng-binding.ng-scope.label-INITIAL')).count().then(function(wv){
				console.log('This client have ' +wv+' wave.');
				for(let n=1;n<=wave_num;n++){
					var elem = element(by.cssContainingText('.list-item.ng-binding.ng-scope.label-INITIAL',waveid));
					var target = element(by.id('sortable-listD'));
					
					//Drag wave from Pending list box and Drop to Manage list box
					browser.actions().dragAndDrop(elem,target).perform();
					
				}
				browser.sleep(5000);
				//Check element in Manage list box
				var mlbox = element.all(by.xpath('//*[@id="sortable-listD"]/li')).get(0);
				expect(mlbox.getText()).toContain(waveid);
				
				screenshots.takeScreenshot(tc+'-'+i+'-2. '+'Run Wave');
				//Click Run button
				element(by.css('[ng-click="runQueue()"]')).click();
			});
		});
		await console.log("Plan Wave : Success");
	};
	//Check Status in page Queue management
	this.check_plan = async function(cli_ent,tc,i){
		
		//Select Client
		await element(by.xpath('//*[@id="tabstrip-1"]/form/div[1]/div/div/span/span/span[1]')).click();
		await browser.sleep(2000);
		let selectcli_ent = element.all(by.css('li.k-item.ng-scope')).get(cli_ent-1);
		await selectcli_ent.click().then(function(){
			browser.sleep(30000);
			screenshots.takeScreenshot(tc+'-'+i+'. '+'Check Wave in Processing list');
			var table = element.all(by.id('processingGrid'));
			var row = table.all(by.css('tr.ng-scope')).count().then(function(cnt){
				var item = cnt;
				console.log('Number of wave in Processing list is ' + item);
				if(cnt != 0){
					var wave_id_q = element(by.xpath('//*[@id="processingGrid"]/table/tbody/tr/td[1]'));
					var wave_st_q = element(by.xpath('//*[@id="processingGrid"]/table/tbody/tr/td[2]'));
					wave_id_q.getText().then(function(text){
						console.log("ID of wave is \'"+text+"\'");
					});
					wave_st_q.getText().then(function(text){
						console.log("Status of wave is \'"+text+"\'");	
						
					});
				};
			});
			
		});
		
	};
	//Get table row
	this.getrow = async function(){
		var table = element.all(by.id('processingGrid'));
		return table.all(by.css('tr.ng-scope')).count();
	};
	
	//Get status in Queue management
	this.get_qm = async function(){
		var st_qm = element.all(by.binding('dataItem.status'));
		return st_qm.getText();
	};

};
module.exports = new webpage();
