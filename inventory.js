// Inventory.js
browser.driver.manage().window().maximize();
var screenshots = require('protractor-take-screenshots-on-demand');
/*<--------------------------------------------------------------Need to change-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->*/

var Test_round = '1'; //Testing round
var Test_date = '03122021'; //Test date
var delete_data = 'y'; //Delete data after do that work. [y:Yes]

//For Inventory : Stock Tracking & Track and Trace
var client_index = '3'; //Client index [0:ALL, 1:Chester Grill, 2:PTTPLC ( AMAZON ), 3:PTTOR Cafe AMAZON Claim, 4:INTHANIN, 5:PTTOR Cafe AMAZON]
var pd_code = '1000194'; //Product code
var PT_ID = ''; //Pallet Tag ID
var lot_no = '';//Lot number
var SN_no = ''; //Serial number
var wh_index = '0'; //Warehouse index [0:ALL, 1:WH_1, 2:WH_2, 3:WH_7, 4:WH_8, 5:WH_9, 6:WH_F, 7:WH_CD, 8:WH_12A, 9:WH_T]
var Aisle = '';//Location aisle
var Bay = ''; //Location bay
var Level = '';//Location level
var Keyword = '14';//Keyword

//For Inventory : Confirm Put Away Location
var AA_Doc = 'AA987189'; //Search Document No.
var s_ai = ''; //Aisle
var s_ba = ''; //Bay
var s_le = ''; // Level

//For Inventory : Create Transfer Request
var mm_date = '03/12/2021'; //Movement Date
var mm_type = '1'//Movement type index
var pro = '1000059'; //Product code
var wh_from = '1' // From Warehouse
var x_from = '' // From Aisle
var y_from = '' // From Bay
var z_from = '' // From Level
var wh_to = '1' // Transfer to Warehouse
var x_to = '' // Transfer to Aisle
var y_to = '' // Transfer to Bay
var z_to = '' // Transfer to Level
var st_PID_from = ''; // From Storage Pallet id
var st_PID_to = ''; // Transfer to Storage Pallet id
var qty_from = '1'; //From Qty
var su_qty_from = ''; //From SU Qty
var new_lot = 'test'; //Create new lot
var exp_new_lot = '03/12/2021'; //EXP date new lot
var mfg_new_lot = '03/12/2021'; //MFG date new lot

/*<--------------------------------------------------------------No need to change-------------------------------------------------------------->*/
var link = 'http://localhost:8000/wms-web/'; //link
var link_TWMS = link + '#/dashboard'; //link TWMS Dashboard page
var username = 'consult';//Username
var passwd = 'consult';//Password
var account = 'Consult Derive'//User Account
var Title = 'TWMS | Warehouse Management System';//Title
var client_name = 'PTTOR Cafe AMAZON'; //Client name
var client_path = 'li[4]'; //Client path : PTTOR Cafe AMAZON
var ship_met = 'li[2]'; //Shipping method path : TR04-Delivery charge
var path_st_INITIAL = '//*[@id="i_status_listbox"]/li[1]'; //Status : INITIAL
var path_st_CONFIRMED = '//*[@id="i_status_listbox"]/li[3]'; //Status : CONFIRMED
var ref_PO_No = 'Auto_TEST_' + Test_round +'_'+ Test_date;//Ref PO No.
var PO_No = 'Auto_TEST_' + Test_round +'_'+ Test_date;//PO No.
var doc_no = 'Auto_TEST_' + Test_round+'_'+ Test_date;//Doc. No.
var ref_doc_no = 'Auto_TEST_' + Test_round +'_'+ Test_date;//Ref Doc No.
var sup = '90002';//Supplier Code
var pro_num = '1'; //Quantity of product item in PO
var pd = ['1000194','1000060','1000059'];//Product Code in PO
var pd_name = ['วัตถุดิบผงโกโก้ Amazon (5 KG.)','แก้วกระเบื้องพร้อมจานรอง 3.5 oz.(6 ชุด)','แก้วกระเบื้องพร้อมจานรอง 7 oz.(6 ชุด)']; //Product name in PO
var qty_pd = ['100','50','100'];//Quantity for each product in PO
var so_pro_num = '1'; //Quantity of product item in SO
var so_pd = ['1000194','1000060','1000059'];//Product Code in SO
var so_pd_name = ['วัตถุดิบผงโกโก้ Amazon (5 KG.)','แก้วกระเบื้องพร้อมจานรอง 3.5 oz.(6 ชุด)','แก้วกระเบื้องพร้อมจานรอง 7 oz.(6 ชุด)']; //Product name in SO
var so_qty_pd = ['10','50','100'];//Quantity for each product in SO

var webpage = require('./webpage.js');

//TC_007 : ทดสอบการแสดงผล Stock Tracking
//TC_007 : Test for search Stock Tracking
/*describe('TC_007 : Test for search Stock Tracking', function () {
    browser.waitForAngularEnabled(false);
	
	it('Search Stock Tracking',async function () {
		
		//Access Link TWMS
		await webpage.getlink(link_TWMS);
		
		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);
		
		await console.log('*-----TC_007 : Test for search Stock Tracking -> Testing Start-----*');
		
		let i = 1;
		let tc = 'TC_007';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);
		
		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();
		
		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(2000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);
		
		i++;
		await console.log('Step '+i+' : Go to page Stock Tracking');
		//Go to page Stock Tracking
		await browser.sleep(2000);
		await webpage.STtrack_page(tc,i);
		
		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Stock Tracking');
		
		//Stock Tracking search
		await browser.sleep(2000);
		i++;
		await console.log('Step '+i+' : Stock Tracking search');
		await webpage.sttrack_search(client_index,pd_code,PT_ID,lot_no,SN_no,tc,i);
		
		//Check data on first row
		await browser.sleep(10000);
		i++;
		await console.log('Step '+i+' : Check data table');
		await webpage.sttrack_check(client_index,pd_code,PT_ID,lot_no,SN_no,tc,i);
		
		//Log out
		await browser.sleep(2000);
		i++;
		await console.log('Step '+i+' : Log out');
		await webpage.getout(tc,i);
		
		await console.log('*-----TC_007 : Test for search Stock Tracking -> Testing Finished-----*');
		
	});	
});

//TC_008 : ทดสอบการแสดงผล Track and Trace
//TC_008 : Test for search Track and Trace
describe('TC_008 : Test for search Track and Trace', function () {
    browser.waitForAngularEnabled(false);
	
    it('Search Track and Trace',async function () {
       
	    //Access Link TWMS
		await webpage.getlink(link_TWMS);
		
		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);
		
		await console.log('*-----TC_008 : Test for search Track and Trace -> Testing Start-----*');
		
		let i = 1;
		let tc = 'TC_008';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);
		
		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();
		
		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(2000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);
		
		i++;
		await console.log('Step '+i+' : Go to page Track and Trace');
		//Go to page Track and Trace
		await browser.sleep(2000);
		await webpage.tat_page(tc,i);
		
		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Track and Trace');
		
		i++;
		await console.log('Step '+i+' : Track and Trace search');
		//Track and Trace search
		await browser.sleep(2000);
		await webpage.tat_search(client_index,wh_index,pd_code,Aisle,Bay,Level,Keyword,tc,i);
		
		//Check data on first row
		await browser.sleep(8000);
		i++;
		await console.log('Step '+i+' : Check data table');
		await webpage.tat_check(client_index,wh_index,pd_code,Aisle,Bay,Level,Keyword,tc,i);
		
		//Log out
		await browser.sleep(3000);
		i++;
		await console.log('Step '+i+' : Log out');
		await webpage.getout(tc,i);
		
		await console.log('*-----TC_008 : Test for search Track and Trace -> Testing Finished-----*');
    });
	
});

//TC_009 : ทดสอบการ Confirm Put Away Location(จิ้มโล)
//TC_009 : Test for Confirm Put Away Location
describe('TC_009 : Test for Confirm Put Away Location', function () {
    browser.waitForAngularEnabled(false);
	
    it('Confirm Put Away Location',async function () {
        
		//Access Link TWMS
		await webpage.getlink(link_TWMS);
		
		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);
		
		await console.log('*-----TC_009 : Test for Confirm Put Away Location -> Testing Start-----*');
		
		let i = 1;
		let tc = 'TC_009';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);
		
		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();
		
		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(2000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);
		
		i++;
		await console.log('Step '+i+' : Go to page Confirm Put Away Location');
		//Go to page Confirm Put Away Location
		await browser.sleep(2000);
		await webpage.CPAL_page(tc,i);
		
		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Confirm Put Away Location');
		
		i++;
		await console.log('Step '+i+' : Locator not available search');
		//Locator not available search
		await browser.sleep(2000);
		await webpage.lna_search(AA_Doc);
		
		//Expect check Status in search page 
		await browser.sleep(5000);
		expect(await webpage.cpal_st_search(tc,i)).toEqual('LOCATOR_NOT_AVAILABLE');
		
		//Confirm put away location
		i++;
		await console.log('Step '+i+' : Select location to put away');
		await webpage.CPAL(s_ai,s_ba,s_le,tc,i);
		
		//Locator not available search
		await browser.sleep(2000);
		i++;
		await console.log('Step '+i+' : Put away search');
		await webpage.pa_search(AA_Doc);
		
		//Expect check Status in search page 
		await browser.sleep(10000);
		expect(await webpage.cpal_st_search(tc,i)).toEqual('PUT_AWAY');
		
		//Log out
		await browser.sleep(3000);
		i++;
		await console.log('Step '+i+' : Log out');
		await webpage.getout(tc,i);
		
		await console.log('*-----TC_009 : Test for Confirm Put Away Location -> Testing Finished-----*');
    
    });
});*/

//TC_010 : ทดสอบการ Create Transfer Request
//TC_010 : Test for Create Transfer Request
describe('TC_010 : Test for Create Transfer Request', function () {
    browser.waitForAngularEnabled(false);
	
    it('Create Transfer Request',async function () {
        
		//Access Link TWMS
		await webpage.getlink(link_TWMS);
		
		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);
		
		await console.log('*-----TC_010 : Test for Create Transfer Request -> Testing Start-----*');
		
		let i = 1;
		let tc = 'TC_010';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);
		
		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();
		
		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(2000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);
		
		i++;
		await console.log('Step '+i+' : Go to page Create Transfer Request');
		//Go to page Create Transfer Request
		await browser.sleep(2000);
		await webpage.CTR_page(tc,i);
		
		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Create Transfer Request');
		
		//Add transfer request
		await browser.sleep(2000);
		i++;
		await console.log('Step '+i+' : Create Transfer Request');
		await webpage.CTR_form(client_path,pro,mm_date,mm_type,wh_from,x_from,y_from,z_from,wh_to,x_to,y_to,z_to,qty_from,su_qty_from,tc,i);
		
		//Transfer Request search
		await browser.sleep(10000);
		i++;
		await console.log('Step '+i+' : Transfer Request search');
		await webpage.tfl_search(client_path,pro,mm_date,mm_type,tc,i);
		
		//Expect check Status in search page 
		await browser.sleep(10000);
		expect(await webpage.tfl_st_search(tc,i)).toEqual('INITIAL');
		
		//Delete Transfer Line
		if(delete_data == 'y'){
			i++;
			console.log('Step '+i+' : Delete Transfer Request');
			webpage.tfl_delete(tc,i);
		}
		
		//Log out
		await browser.sleep(8000);
		i++;
		await console.log('Step '+i+' : Log out');
		await webpage.getout(tc,i);
		
		await console.log('*-----TC_010 : Test for Create Transfer Request -> Testing Finished-----*');
    
    });
	
});
