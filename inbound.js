// Inbound.js
browser.driver.manage().window().maximize();
var screenshots = require('protractor-take-screenshots-on-demand');
/*<--------------------------------------------------------------Need to change-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->*/
var currentDate = new Date();
var thisDay = currentDate.getDate() + '' + (currentDate.getMonth() + 1) + currentDate.getFullYear();
var thisDayWithSlash = currentDate.getDate() + '/' + (currentDate.getMonth() + 1) + '/' + currentDate.getFullYear();
var thisDayLastYearWithSlash = currentDate.getDate() + '/' + (currentDate.getMonth() + 1) + '/' + (currentDate.getFullYear()-1);
var Test_round = currentDate.getHours()+''+currentDate.getMinutes()+currentDate.getMilliseconds(); //Testing round
var Test_date = thisDay; //Test date
var delete_data = 'y'; //Delete data after do that work. [y:Yes]

//For Inbound : Purchase Orders
var doc_date = '03/12/2021'; //Document Date
var del_date = '03/12/2021'; //Delivery Date

//For Inbound : Arrival Advices
var doc_date_cal = "document.getElementById('i_document_date_edit').value='03/12/2021'"; //Doc date for AA

//For Inbound : Product Receipt
var typ = 'r'; // Case for receive [r:received, c:completed]
var mfg_pd = ['03/12/2021','03/12/2021','03/12/2021']; //Manufacture date (MFG)
var exp_pd = ['03/12/2022','03/12/2022','03/12/2022']; //Expiry date (EXP)
var exp_pd_re = ['09/07/2022','19/07/2022','19/07/2022']; //Expiry date (EXP) : RECEIVED
var val_st = '1'; // Index of status for product receipt [1:GOOD, 2:DM, 3:WIP]
var mode = 'ad'; //Product receipt adjust or reject [ad:Adjust, re:Reject]

//For Inbound : Print Pallet Tag
var AA_PT = ''; //AA Doc no/Pallet Tag Search

/*<--------------------------------------------------------------No need to change-------------------------------------------------------------->*/
var link = 'http://wms.tws.solutions/twms-test-dashboard/'; //link
var link_TWMS = link + '#/dashboard'; //link TWMS Dashboard page
var username = 'consult';//Username
var passwd = 'consult';//Password
var account = 'Consult Derive'//User Account
var Title = 'TWMS | Warehouse Management System';//Title
var client_name = 'PTTOR Cafe AMAZON'; //Client name
var client_path = 'li[4]'; //Client path : PTTOR Cafe AMAZON
var ship_met = 'li[2]'; //Shipping method path : TR04-Delivery charge
var path_st_INITIAL = '//*[@id="i_status_listbox"]/li[1]'; //Status : INITIAL
var path_st_CONFIRMED = '//*[@id="i_status_listbox"]/li[3]'; //Status : CONFIRMED
var ref_PO_No = 'Auto_TEST_' + Test_round +'_'+ Test_date;//Ref PO No.
var PO_No = 'Auto_TEST_' + Test_round +'_'+ Test_date;//PO No.
var doc_no = 'Auto_TEST_' + Test_round+'_'+ Test_date;//Doc. No.
var ref_doc_no = 'Auto_TEST_' + Test_round +'_'+ Test_date;//Ref Doc No.
var sup = '90002';//Supplier Code
var pro_num = '1'; //Quantity of product item in PO
var pd = ['1000194','1000060','1000059'];//Product Code in PO
var pd_name = ['วัตถุดิบผงโกโก้ Amazon (5 KG.)','แก้วกระเบื้องพร้อมจานรอง 3.5 oz.(6 ชุด)','แก้วกระเบื้องพร้อมจานรอง 7 oz.(6 ชุด)']; //Product name in PO
var qty_pd = ['100','50','100'];//Quantity for each product in PO

var webpage = require('./webpage.js');

//TC_001 : ทดสอบการ Login เข้าสู่ระบบ T-WMS
//TC_001 : Test Login to T-WMS
describe('TC_001 : Test Login to T-WMS', function () {
	browser.waitForAngularEnabled(false);
	it('Log in & Log Out', async function(){

		//Access Link TWMS
		await webpage.getlink(link_TWMS);

		//Expect Title : TWMS | Warehouse Management System
        await expect(browser.getTitle()).toEqual(Title);

		await console.log('*-----TC_001 : Test Login to T-WMS -> Testing Start-----*');

		let i = 1;
		let tc = 'TC_001_Step_';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);

		//click Login button
		await browser.sleep(1000);
		await webpage.loginclick();

		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(2000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);

		i++;
		await console.log('Step '+i+' : Log out');
		//Log out
		await browser.sleep(2000);
		await webpage.getout(tc,i);

		await console.log('*-----TC_001 : Test Login to T-WMS -> Testing Finished-----*');
	});
});

//TC_002 : ทดสอบการสร้างเอกสาร Purchase Order
//TC_002 : Test for create Purchase Order
describe('TC_002 : Test for create Purchase Order', function () {
	browser.waitForAngularEnabled(false);
	it('Create Purchase Order', async function(){

		//Access Link TWMS
		await webpage.getlink(link_TWMS);

		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);

		await console.log('*-----TC_002 : Test for create Purchase Order : Testing Start-----*');

		let i = 1;
		let tc = 'TC_002_Step_';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);

		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();

		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(3000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);

		i++;
		await console.log('Step '+i+' : Go to page Purchase Orders');
		//click inbound
		await browser.sleep(2000);
		await element.all(by.linkText('Inbound')).click();

		//Go to page Purchase Orders
		await webpage.PO_page(tc,i);

		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Purchase Orders');
		await console.log('This is Page Purchase Orders');

		//wait for data table : order by sequence from create po
		await browser.sleep(10000);

		i++;
		await console.log('Step '+i+' : Create Purchase Orders');
		//Create Purchase Orders => INITIAL
		await webpage.create_PO_initial(client_path,ref_PO_No,PO_No,doc_date,del_date,sup,pro_num,pd,qty_pd,tc,i);

		//Search PO : Status => INITIAL
		await browser.sleep(10000);
		await webpage.search_PO_IN(path_st_INITIAL,PO_No);

		//Expect check PO No. on table
		await browser.sleep(10000);
		expect(await webpage.get_PO(tc,i,3)).toEqual(PO_No);
		//Expect check Status on table
		expect(await webpage.get_st_PO()).toEqual('INITIAL');

		i++;
		await console.log('Step '+i+' : Confirm Purchase Orders');
		//Confirm PO -> CONFIRMED
		await browser.sleep(5000);
		await webpage.confirm_PO(tc,i);

		//Search PO : Status => CONFIRMED
		await browser.sleep(10000);
		await webpage.search_PO_CON(path_st_CONFIRMED,PO_No);

		//Expect check PO No. on table
		await browser.sleep(10000);
		expect(await webpage.get_PO(tc,i,2)).toEqual(PO_No);
		//Expect check Status on table
		expect(await webpage.get_st_PO()).toEqual('CONFIRMED');

		i++;
		await console.log('Step '+i+' : Log out');
		//Log out
		await browser.sleep(2000);
		await webpage.getout(tc,i);

		await console.log('*-----TC_002 : Test for create Purchase Order : Testing Finished-----*');
	});

});

//TC_003 : ทดสอบการสร้างเอกสาร Arrival Advices
//TC_003 : Test for create Arrival Advices
describe('TC_003 : Test for create Arrival Advices', function () {
    browser.waitForAngularEnabled(false);

	it('Create Arrival Advices',async function () {

		//Access Link TWMS
		await webpage.getlink(link_TWMS);

		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);

		await console.log('*-----TC_003 : Test for create Arrival Advices -> Testing Start-----*');

		let i = 1;
		let tc = 'TC_003_Step_';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);

		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();

		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(2000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);

		i++
		await console.log('Step '+i+' : Go to page Arrival Advices');
		//click inbound
		await browser.sleep(2000);
		await element.all(by.linkText('Inbound')).click();

		//Go to page Arrival Advices
		await browser.sleep(1000);
		await webpage.AA_page(tc,i);

		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Arrival Advice');
		await console.log('This is Page Arrival Advice');

		//wait for data table : order by sequence from create AA
		await browser.sleep(10000);

		i++;
		await console.log('Step '+i+' : Create Arrival Advices');
		//Create Arrival Advices => AA_INITIAL
		await webpage.create_AA_initial(client_path,doc_no,sup,doc_date_cal,PO_No,tc,i);

		//Search AA : Status => AA_INITIAL
		await browser.sleep(8000);
		await webpage.search_AA_CON(path_st_CONFIRMED,doc_no);

		//Expect check AA on table
		await browser.sleep(10000);
		expect(await webpage.get_doc(tc,i,3)).toEqual(doc_no);
		//Expect check Status on table
		expect(await webpage.get_st_AA()).toEqual('AA_INITIAL');

		i++;
		await console.log('Step '+i+' : Log out');
		//Log out
		await browser.sleep(2000);
		await webpage.getout(tc,i);

		await console.log('*-----TC_003 : Test for create Arrival Advices -> Testing Finished-----*');

    });

});

//TC_004 : ทดสอบการรับเข้าสินค้า Product Receipt ผ่านหน้าเว็บ
//TC_004 : Test for Product Receipt on web
describe('TC_004 : Test for Product Receipt on web', function () {
    browser.waitForAngularEnabled(false);

	it('Product Receipt',async function () {

		//Access Link TWMS
		await webpage.getlink(link_TWMS);

		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);

		await console.log('*-----TC_004 : Test for Product Receipt on web -> Testing Start-----*');

		let i = 1;
		let tc = 'TC_004_Step_';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);

		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();

		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(2000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);

		i++;
		await console.log('Step '+i+' : Go to page Product Receipt');
		//click inbound
		await browser.sleep(2000);
		await element.all(by.linkText('Inbound')).click();

		//Go to page Product Receipt
		await browser.sleep(1000);
		await webpage.PR_page(tc,i);

		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Product Receipt');
		await console.log('This is Page Product Receipt');

		i++;
		await console.log('Step '+i+' : Search AA for Product Receive');
		//Search AA for Product Receive
		await webpage.search_PR_AA(doc_no,doc_date);

		//Check Status on table
		await browser.sleep(10000);
		await webpage.PR_st_search(tc,i);

		i++;
		await browser.sleep(2000);
		await console.log('Step '+i+' : Product Receive on web');
		if(typ == 'r'){
			//Product Receipt received on web
			await webpage.pr_received(mfg_pd,exp_pd_re,doc_no,val_st,tc,i);
		}
		else if(typ == 'c'){
			//Product Receipt completed on web
			await webpage.pr(mfg_pd,exp_pd,doc_no,val_st,tc,i);
		}

		i++;
		await console.log('Step '+i+' : Search AA and check status');
		//Search AA after Product Receipt
		await browser.sleep(8000);
		await webpage.search_PR_all(doc_no,doc_date);

		//Check Status on table
		await browser.sleep(30000);
		await webpage.PR_st_search(tc,i);

		i++;
		await console.log('Step '+i+' : Log out');
		//Log out
		await browser.sleep(2000);
		await webpage.getout(tc,i);

		await console.log('*-----TC_004 : Test for Product Receipt on web -> Testing Finished-----*');

	});

});

//TC_005 : ทดสอบการ Adjust หรือ Reject รับเข้าสินค้า : กรณีที่ติดสถานะ Received
//TC_005 : Test for adjust or reject product receipt in case Status is RECEIVED
describe('TC_005 : Test for adjust or reject product receipt in case Status is RECEIVED', function () {
    browser.waitForAngularEnabled(false);

	it('Product Adjust or Reject',async function () {

		//Access Link TWMS
		await webpage.getlink(link_TWMS);

		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);

		await console.log('*-----TC_005 : Test for adjust or reject product receipt in case Status is RECEIVED -> Testing Start-----*');

		let i = 1;
		let tc = 'TC_005_Step_';
		await console.log('Step '+ i +' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);

		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();

		i++;
		await console.log('Step '+ i +' : Check User account');
		//Expect Account
		await browser.sleep(2000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);

		i++;
		await console.log('Step '+ i +' : Go to page Product Receipt');
		//click inbound
		await browser.sleep(2000);
		await element.all(by.linkText('Inbound')).click();

		//Go to page Product Receipt
		await browser.sleep(1000);
		await webpage.PR_page(tc,i);

		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Product Receipt');
		await console.log('This is Page Product Receipt');

		i++;
		await console.log('Step '+ i +' : Search AA and check status');
		//Search AA : Status => RECEIVED
		await webpage.search_PR_received(doc_no);

		//wait for data table
		await browser.sleep(30000);

		//Expect check Status Product Receipt in search page
		expect(await webpage.PR_st_search(tc,i)).toEqual('RECEIVED');

		i++;
		await console.log('Step '+ i +' : Product Adjust or Reject');
		//Adjust or Reject Product Receipt
		await browser.sleep(5000);
		await webpage.ad_re(mode,account,Test_date,tc,i);

		if(mode == 'ad'){
			//Expect check Status in AA detail page
			await browser.sleep(30000);
			expect(await webpage.PR_st_detail(tc,i,5)).toEqual('RECEIVE_CONFIRMED');
			await browser.sleep(2000);

			//Back to search page
			await element.all(by.buttonText('Back')).first().click();

		}

		await browser.sleep(20000);
		await browser.refresh();
		i++;
		await console.log('Step '+ i +' : Search AA and check status');
		//Search AA Status
		await browser.sleep(20000);
		await webpage.search_PR_all(doc_no);

		//wait for data table
		await browser.sleep(25000);

		//Expect check Status Product Receipt in search page
		if(mode == 'ad'){
			expect(await webpage.PR_st_search(tc,i)).toEqual('COMPLETED');
		}
		else if(mode == 're'){
			expect(await webpage.PR_st_search(tc,i)).toEqual('AA_INITIAL');
		}

		i++;
		await console.log('Step '+ i +' : Log out');
		//Log out
		await browser.sleep(2000);
		await webpage.getout(tc,i);

		await console.log('*-----TC_005 : Test for adjust or reject product receipt in case Status is RECEIVED -> Testing Finished-----*');
    });

});

//TC_006 : ทดสอบการแสดงผล Print Pallet Tag
//TC_006 : Test for search Print Pallet Tag
describe('TC_006 : Test for search Print Pallet Tag', function () {
    browser.waitForAngularEnabled(false);

	it('Get AA number to search Pallet tags',async function () {

		//Access Link TWMS
		await webpage.getlink(link_TWMS);

		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);

		await console.log('*-----TC_006 : Test for search Print Pallet Tag -> Testing Start-----*');

		let i = 1;
		let tc = 'TC_006_Step_';
		await console.log('Step '+ i +' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);

		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();

		i++;
		await console.log('Step '+ i +' : Check User account');
		//Expect Account
		await browser.sleep(2000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);

		//click inbound
		await browser.sleep(2000);
		await element.all(by.linkText('Inbound')).click();

		if(AA_PT == ''){

			i++;
			await console.log('Step '+ i +' : Go to page Product Receipt');
			//Go to page Product Receipt
			await browser.sleep(1000);
			await webpage.PR_page(tc,i);

			//Expect check Page
			await browser.sleep(2000);
			var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
			expect(await page_title.getText()).toEqual('Product Receipt');
			await console.log('This is Page Product Receipt');

			i++;
			await console.log('Step '+ i +' : Search AA');
			//Search AA : Status => COMPLETED
			await browser.sleep(5000);
			await webpage.search_PR_completed(doc_no);

			//wait for data table
			await browser.sleep(20000);

			//Expect check Status Product Receipt in search page
			expect(await webpage.PR_st_search(tc,i)).toEqual('COMPLETED');

			//Get AA number
			AA_PT = await webpage.get_aa();

		}

		i++;
		await console.log('Step '+ i +' : Go to page Print Pallet Tag');
		//Go to page Print Pallet Tag
		await browser.sleep(1000);
		await webpage.PT_page(tc,i);

		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Print Pallet Tag');
		await console.log('This is Page Print Pallet Tag');

		i++;
		await console.log('Step '+ i +' : Search Pallet Tag');
		//Search Pallet Tag in AA Doc no/Pallet Tag
		await webpage.search_PT(AA_PT);
		await browser.sleep(30000);

		//Expect String in data table to contain AA number
		await screenshots.takeScreenshot(tc+'-'+i+'. '+'Search Pallet tag');
		await element.all(by.xpath('//*[@id="printGrid"]/div[1]/a[4]')).click();
		await webpage.expect_PT(AA_PT);

		i++;
		await console.log('Step '+ i +' : Click Print pallet tag and check new tab');
		//Click Print pallet tag and check new tab
		var PT_number = await webpage.get_pt();
		await webpage.print_pt(link,PT_number,tc,i);

		//Log out
		await browser.sleep(2000);
		i++;
		await console.log('Step '+ i +' : Log out');
		await webpage.getout(tc,i);

		await console.log('*-----TC_006 : Test for search Print Pallet Tag -> Testing Finished-----*');

    });

});
