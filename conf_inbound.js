// conf1.js
var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');
var screenshots = require('protractor-take-screenshots-on-demand');

var reporter = new HtmlScreenshotReporter({
	dest: 'target/screenshots_Inbound',
	filename: 'my-report.html'
});

exports.config = {
    framework: 'jasmine',
    capabilities: {
              'browserName': 'chrome',

						  chromeOptions: {
						     args: [ '--no-sandbox','--disable-dev-shm-usage',"--headless", "--disable-gpu", "--window-size=800,600" ]
						   }
    },
    //กำหนดให้ Selenium Server ทำ Action
    //seleniumAddress: 'http://localhost:4444/wd/hub',
     //กำหนดให้เรียก webdriver บน browser ทำ Action
    directConnect: true,
    specs: ['inbound.js'] ,//Name of the file containing test cases

    // Options to be passed to Jasmine.
    jasmineNodeOpts: {
          defaultTimeoutInterval: 2000000
    },

	// Setup the report before any tests start
	beforeLaunch: function() {
		return new Promise(function(resolve){
			reporter.beforeLaunch(resolve);
		});
	},

	onPrepare: function() {

		// Assign the test reporter to each running instance
		jasmine.getEnv().addReporter(reporter);

		//joiner between browser name and file name
		screenshots.browserNameJoiner = ' - ';//this is the default

		//folder of screenshots
		screenshots.screenShotDirectory = 'target/screenshots_Inbound';

		//creates folder of screenshots
		screenshots.createDirectory();

	},

	// Close the report after all tests finish
	afterLaunch: function(exitCode) {
		return new Promise(function(resolve){
			reporter.afterLaunch(resolve.bind(this, exitCode));
		});
	}

}
