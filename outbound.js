// spec_Outbound.js
browser.driver.manage().window().maximize();
var screenshots = require('protractor-take-screenshots-on-demand');
/*<--------------------------------------------------------------Need to change-------------------------------------------------------------->*/

var Test_round = '1'; //Testing round
var Test_date = '03122021'; //Test date
var delete_data = 'y'; //Delete data after do that work. [y:Yes]

//For Outbound : Sales Orders
var customer = 'ILS'; // Customer
var ship_to = 'li[2]'; // Ship to address path
var so_date = '03/12/2021'; // SO Date
var rq_date = '03/12/2021'; // Request Date

//For Outbound : Wave
var new_wave = 'y'; //Do you want to create new wave? [y:Yes, n:No]
var so_num = '1'; //How many SO you want to add on wave?
var c_index = '5'; // Client index [1:INTHANIN, 2:PTTOR Cafe AMAZON, 3:Chester Grill, 4:PTTPLC ( AMAZON ), 5:PTTOR Cafe AMAZON Claim]
//var c_index = '3'; // Client index for localhost [1:INTHANIN, 2:PTTOR Cafe AMAZON, 3:Chester Grill, 4:PTTPLC ( AMAZON ), 5:PTTOR Cafe AMAZON Claim]
var wh_pick = ''; // Warehouse
var w_config = '40'; // Wave Config dropdown Index
var pk_date = '07/12/2021'; // Request Date
var pallet_number = '1';// Pallet Number for check stock
var waveid = '25085';// Wave ID column

//For Outbound : Picking
var ct_index = '4'; // Client index
var dn = ''; //Doc. No. or keyword for search picking

//For Outbound : Queue management
var cli_ent = '3'; // Client index [1:INTHANIN, 2:PTTOR Cafe AMAZON, 3:Chester Grill, 4:PTTPLC ( AMAZON ), 5:PTTOR Cafe AMAZON Claim]
//var cli_ent = '3'; // Client index [1:INTHANIN, 2:PTTOR Cafe AMAZON, 3:Chester Grill, 4:PTTPLC ( AMAZON ), 5:PTTOR Cafe AMAZON Claim]
var wave_num = '1'; //How many wave do you want to plan?
var sw = 'y';//Want to plan wave? [y:Yes]

//For Outbound : Assign Job
var picker = 'spAdmin'; //Username for Select Picker
var client_job = '4'; // Client index for Select Picking [1:ALL, 2:INTHANIN, 3:PTTOR Cafe AMAZON, 4:Chester Grill, 5:PTTPLC ( AMAZON ), 6:PTTOR Cafe AMAZON Claim]
var pk_status = '3'; // Pick Status index [1:ALL, 2:IN_PROGRESS, 3:RELEASED]
var pick_date = '03/11/2021'; //Pick Date for Select Picking
var job_keyword = ''; //Keyword for Select Picking
var job = '1'; //Number of job that want to assign

/*<--------------------------------------------------------------No need to change-------------------------------------------------------------->*/
var link = 'http://pm2.derive.co.th/twms-test/'; //link for pm2 test server
//var link = 'http://localhost:8000/wms-web/'; // link for localhost
var link_TWMS = link + '#/dashboard'; //link TWMS Dashboard page
var username = 'consult';//Username
var passwd = 'consult';//Password
var account = 'Consult Derive'//User Account
var Title = 'TWMS | Warehouse Management System';//Title
var client_name = 'PTTOR Cafe AMAZON'; //Client name
var client_path = 'li[4]'; //Client path : PTTOR Cafe AMAZON
var ship_met = 'li[2]'; //Shipping method path : TR04-Delivery charge
var path_st_INITIAL = '//*[@id="i_status_listbox"]/li[1]'; //Status : INITIAL
var path_st_CONFIRMED = '//*[@id="i_status_listbox"]/li[3]'; //Status : CONFIRMED
var ref_PO_No = 'Auto_TEST_' + Test_round +'_'+ Test_date;//Ref PO No.
var PO_No = 'Auto_TEST_' + Test_round +'_'+ Test_date;//PO No.
var doc_no = 'Auto_TEST_' + Test_round+'_'+ Test_date;//Doc. No.
var ref_doc_no = 'Auto_TEST_' + Test_round +'_'+ Test_date;//Ref Doc No.
var sup = '90002';//Supplier Code
var so_pro_num = '3'; //Quantity of product item in SO
var so_pd = ['1000061','1000063','1000141'];//Product Code in SO
var so_qty_pd = ['10','10','10'];//Quantity for each product in SO

var webpage = require('./webpage.js');

//TC_011 : ทดสอบการสร้างเอกสาร Sales Order
//TC_011 : Test for Create Sales Order
/*describe('TC_011 : Test for Create Sales Orders', function () {
    browser.waitForAngularEnabled(false);
	
    it('Create Sales Orders',async function () {
        //Access Link TWMS
		await webpage.getlink(link_TWMS);
		
		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);
		
		await console.log('*-----TC_011 : Test for Create Sales Orders -> Testing Start-----*');
		
		let i = 1;
		let tc = 'TC_011';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);
		
		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();
		
		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(30000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);
		
		i++;
		await console.log('Step '+i+' : Go to page Sales Orders');
		//Go to page Sales Orders
		await browser.sleep(2000);
		await webpage.SO_page(tc,i);
		
		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Sales Order');
		
		//Click Create SO
		await browser.sleep(2000);
		i++;
		await console.log('Step '+i+' : Create Sales Orders');
		await webpage.create_so(client_path,ship_met,customer,ship_to,so_date,rq_date,ref_doc_no,so_pro_num,so_pd,so_qty_pd,tc,i);
		
		//Sales Orders search
		await browser.sleep(60000);
		i++;
		await console.log('Step '+i+' : Sales Orders search');
		await webpage.so_st_search(ref_doc_no,rq_date,so_date,tc,i);
		
		//Expect check Status in search page 
		await browser.sleep(600000);
		expect(await webpage.so_st_check(tc,i)).toEqual('CONFIRMED');
		
		//Log out
		await browser.sleep(3000);
		i++;
		await console.log('Step '+i+' : Log out');
		await webpage.getout(tc,i);
		
		await console.log('*-----TC_011 : Test for Create Sales Orders -> Testing Finished-----*');
    
    });
});*/

//TC_012 : ทดสอบการสร้าง Wave
//TC_012 : Test for Create Wave
/*describe('TC_012 : Test for Create Wave', function () {
    browser.waitForAngularEnabled(false);
	
	it('Create Sales Orders',async function () {
        //Access Link TWMS
		await webpage.getlink(link_TWMS);
		
		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);
		
		await console.log('*-----TC_012 : Test for Create Wave -> Testing Start-----*');
		
		let i = 1;
		let tc = 'TC_012';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);
		
		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();
		
		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(5000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);
		
		i++;
		await console.log('Step '+i+' : Go to page Wave');
		//Go to page Wave
		await browser.sleep(2000);
		await webpage.wave_page(tc,i);
		
		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Wave');
		
		//Create Wave
		if(new_wave == 'y'){
			await browser.sleep(2000);
			i++;
			await console.log('Step '+i+' : Create Wave');
			await webpage.create_wave(c_index,wh_pick,w_config,pk_date,tc,i);
			
		}
		
		await browser.sleep(20000);
		//Wave search
		i++;
		await console.log('Step '+i+' : Wave search Created Wave');
		await webpage.wave_st_initial(c_index,wh_pick,pk_date,tc,i);
		
		//Expect check Status in search page 
		await browser.sleep(20000);
		expect(await webpage.wave_st_check(tc,i)).toEqual('INITIAL');
		
		//Add SO in wave
		i++;
		await console.log('Step '+i+' : Add SO in wave');
		await webpage.wave_add_so(so_num,ref_doc_no,rq_date,customer,tc,i);
		
		//Wave search
		await browser.sleep(120000);
		i++;
		await console.log('Step '+i+' : Wave search after Add SO');
		await webpage.wave_st_initial(c_index,wh_pick,pk_date,tc,i);
		
		//Expect check Status in search page 
		await browser.sleep(30000);
		expect(await webpage.wave_st_check(tc,i)).toEqual('INITIAL');
		
		//Log out
		await browser.sleep(3000);
		i++;
		await console.log('Step '+i+' : Log out');
		await webpage.getout(tc,i);
		
		await console.log('*-----TC_012 : Test for Create Wave -> Testing Finished-----*');
    
    });
		   
});

//TC_013 : ทดสอบการเจาะ lot
//TC_013 : Test to find pallet number on wave(เจาะ lot)
describe('TC_013 : Test to find pallet number on wave', function () {
    browser.waitForAngularEnabled(false);
	
    it('Find pallet number on wave',async function () {
        //Access Link TWMS
		await webpage.getlink(link_TWMS);
		
		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);
		
		await console.log('*-----TC_013 : Test to find pallet number on wave -> Testing Start-----*');
		
		let i = 1;
		let tc = 'TC_013';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);
		
		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();
		
		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(5000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);
		
		i++;
		await console.log('Step '+i+' : Go to page Wave');
		//Go to page Wave
		await browser.sleep(2000);
		await webpage.wave_page(tc,i);
		
		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Wave');
		
		await browser.sleep(10000);
		//Wave search
		i++;
		await console.log('Step '+i+' : Wave search');
		await webpage.wave_st_initial(c_index,wh_pick,pk_date,tc,i);
		
		//Expect check Status in search page 
		await browser.sleep(30000);
		expect(await webpage.wave_st_check(tc,i)).toEqual('INITIAL');
		
		//Find Pallet Number on wave
		await browser.sleep(2000);
		i++;
		await console.log('Step '+i+' : Find Pallet Number on wave');
		await webpage.find_pt(pallet_number,tc,i);
		
		await browser.sleep(30000);
		await webpage.ch_find_pt(pallet_number,tc,i);
		
		//Log out
		await browser.sleep(3000);
		i++;
		await console.log('Step '+i+' : Log out');
		await webpage.getout(tc,i);
		
		await console.log('*-----TC_013 : Test to find pallet number on wave -> Testing Finished-----*');
    
    });
});*/

//TC_014 : ทดสอบการ Plan wave
//TC_014 : Test for Plan wave
describe('TC_014 : Test for Plan wave', function () {
	
    browser.waitForAngularEnabled(false);
	
    it('Plan wave',async function () {
		
		//Access Link TWMS
		await webpage.getlink(link_TWMS);
		
		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);
		
		await console.log('*-----TC_014 : Test for Plan wave -> Testing Start-----*');
		
		let i = 1;
		let tc = 'TC_014';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);
		
		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();
		
		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(2000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);
		
		if(waveid == ''){
			
			i++;
			await console.log('Step '+i+' : Go to page Wave');
			//Go to page Wave
			await browser.sleep(2000);
			await webpage.wave_page(tc,i);
		
			//Expect check Page
			await browser.sleep(2000);
			var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
			expect(await page_title.getText()).toEqual('Wave');
		
			await browser.sleep(30000);
			//Wave search
			i++;
			await console.log('Step '+i+' : Wave search');
			await webpage.wave_st_initial(c_index,wh_pick,pk_date,tc,i);
		
			//Expect check Status in search page 
			await browser.sleep(30000);
			expect(await webpage.wave_st_check(tc,i)).toEqual('INITIAL');
		
			//Get Wave ID number
			waveid = await webpage.get_waveid();
			
			await element.all(by.linkText('Outbound')).click();
		}
		
		i++;
		await console.log('Step '+i+' : Go to page Queue management');
		//Go to page Queue management
		await browser.sleep(2000);
		await webpage.QM_page(tc,i);
		
		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Queue Management');
		
		//Plan wave in page Queue management
		if(sw == 'y'){
			i++;
			await console.log('Step '+i+' : Plan wave in page Queue management');
			await browser.sleep(5000);
			await webpage.plan_wave(cli_ent,wave_num,waveid,tc,i);
		}
		await browser.sleep(30000);
		
		//Check Status in page Queue management
		i++;
		await console.log('Step '+i+' : Check Status in page Queue management');
		await webpage.check_plan(cli_ent,tc,i);
		await browser.sleep(30000);
		var stp;
		var p = await webpage.getrow();
		if(p!=0){
			stp = await webpage.get_qm();
		}
		
		i++;
		await console.log('Step '+i+' : Go to page Wave');
		//Click Outbound
		await element.all(by.linkText('Outbound')).click();
		//Go to page Wave
		await browser.sleep(1000);
		await webpage.wave_page(tc,i);
		
		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Wave');
		
		//Wave search
		i++;
		await console.log('Step '+i+' : Wave search');
		await browser.sleep(30000);
		await webpage.wave_st_plan(stp,c_index,wh_pick,pk_date,tc,i);
		
		//Expect check Status in search page
		await browser.sleep(120000);
		if(stp=='PLANNING'){
			expect(await webpage.wave_st_check(tc,i)).toEqual('INITIAL');
		}
		else{
			expect(await webpage.wave_st_check(tc,i)).toEqual('RELEASED');
		}
		
		//View to check status in wave
		i++;
		await console.log('Step '+i+' : SO Status in wave');
		await webpage.ciw(tc,i);
		
		//Log out
		await browser.sleep(10000);
		i++;
		await console.log('Step '+i+' : Log out');
		await webpage.getout(tc,i);
		 
		await console.log('*-----TC_014 : Test for Plan wave -> Testing Finished-----*');
    
    });
});

//TC_015 : ทดสอบการมอบหมายงานใน Assign Job
//TC_015 : Test for Assign Job
/*describe('TC_015 : Test for Assign Job', function () {
    browser.waitForAngularEnabled(false);
	
    it('TC_015 : Test for Assign Job',async function () {
        //Access Link TWMS
		await webpage.getlink(link_TWMS);
		
		//Expect check Title
        await expect(browser.getTitle()).toEqual(Title);
		
		await console.log('*-----TC_015 : Test for Assign Job -> Testing Start-----*');
		
		let i = 1;
		let tc = 'TC_015';
		await console.log('Step '+i+' : Input Username,Password then click Log in');
		//send Username & Password
		await browser.sleep(1000);
		await webpage.name_pass(username,passwd,tc,i);
		
		//Click Login button
		await browser.sleep(1000);
		await webpage.loginclick();
		
		i++;
		await console.log('Step '+i+' : Check User account');
		//Expect Account
		await browser.sleep(2000);
		expect(await webpage.getaccount(tc,i)).toEqual(account);
		
		if(job==1 && pick_date != ''){
			if(job_keyword == ''){
			
				if(dn==''){
				
					i++;
					await console.log('Step '+i+' : Go to page Wave');
					//Go to page Wave
					await browser.sleep(1000);
					await webpage.wave_page(tc,i);
		
					//Expect check Page
					await browser.sleep(2000);
					var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
					expect(await page_title.getText()).toEqual('Wave');
		
					//Wave search
					i++;
					await console.log('Step '+i+' : Wave search');
					await browser.sleep(5000);
					await webpage.wave_st_released(c_index,wh_pick,pick_date,tc,i);
					await browser.sleep(20000);
			
					//Expect check Status Product Receipt in search page 
					expect(await webpage.wave_st_check(tc,i)).toEqual('RELEASED');
		
					//Get Doc. No.
					dn = await webpage.get_docno();
				}
			
				//Go to page Picking
				i++;
				await console.log('Step '+i+' : Go to page Picking');
			
				//Click Outbound
				await element.all(by.linkText('Outbound')).click();
				await browser.sleep(1000);
				await webpage.picking_page(tc,i);
		
				//Expect check Page
				await browser.sleep(2000);
				var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
				expect(await page_title.getText()).toEqual('Picking');
		
				//Picking search
				i++;
				await console.log('Step '+i+' : Picking search');
				await browser.sleep(5000);
				await webpage.picking_search(ct_index,wh_pick,pick_date,dn,tc,i);
				await browser.sleep(20000);
			
				//Expect check Status Product Receipt in search page 
				expect(await webpage.pick_st_check(tc,i)).toEqual('RELEASED');
		
				//Get Doc. No.
				job_keyword = await webpage.get_jobkw();
			
			}
		}
		
		//Go to page Assign Job
		i++;
		await console.log('Step '+i+' : Go to page Assign Job');
		
		//Click Outbound
		if(job == 1 && pick_date != ''){
			await element.all(by.linkText('Outbound')).click();
		}
		await browser.sleep(2000);
		await webpage.AJ_page(tc,i);
		
		//Expect check Page
		await browser.sleep(2000);
		var page_title = element(by.xpath('/html/body/div[1]/div[2]/div[3]/div/ng-controller/div[1]/div[1]/div/h3'));
		expect(await page_title.getText()).toEqual('Assign Job');
		
		//Assign Job
		await browser.sleep(20000);
		i++;
		await console.log('Step '+i+' : Assign Job');
		await webpage.assign_job(picker,client_job,pk_status,pick_date,job_keyword,job,tc,i);
		
		//Delete job
		if(delete_data == 'y'){
			await browser.sleep(20000);
			i++;
			console.log('Step '+i+' : Delete Job');
			webpage.delete_aj(job,tc,i);
		}
		
		//Log out
		await browser.sleep(3000);
		i++;
		await console.log('Step '+i+' : Log out');
		await webpage.getout(tc,i);
		
		await console.log('*-----TC_015 : Test for Assign Job -> Testing Finished-----*');
    });
	
});*/